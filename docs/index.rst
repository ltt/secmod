.. SecMOD documentation master file, created by
   sphinx-quickstart on Thu Apr 25 15:42:33 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SecMOD's documentation!
==================================

.. toctree::
    :maxdepth: 2
    :caption: Contents

    usage/quickstart
    usage/installation
    usage/data
    usage/developer_reference
    usage/packages
    usage/troubleshooting



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
