====================
Packages in SecMOD
====================

.. csv-table::
    :widths: auto
    :header: Package name, Usage, Link

    Pyomo, Optimization, https://www.pyomo.org/
    Numpy, Scientific Computing, https://numpy.org/
    Pandas, Scientific Computing, https://pandas.pydata.org/
    Scipy, Scientific Computing, https://www.scipy.org/
    datapackage, Datamanagement, https://frictionlessdata.io/docs/using-data-packages-in-python/
    geopy, Geocoordinates/distances, https://github.com/geopy/geopy
    pint, Unit-Handling, https://pint.readthedocs.io/en/latest/
    pathlib, , https://docs.python.org/3/library/pathlib.html
    requests, Network, https://3.python-requests.org/
    clint, Terminal-Visualation, https://github.com/kennethreitz/clint
    tikzplotlib, Visualization, https://github.com/nschloe/tikzplotlib
    sympy, , https://www.sympy.org/en/index.html
    tsam, Scientific Computing, https://github.com/FZJ-IEK3-VSA/tsam
    pandastable, Visualization, https://pandastable.readthedocs.io/en/latest/description.html
