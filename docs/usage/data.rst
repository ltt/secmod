==============
Data in SecMOD
==============
Plese refer to our publication for a detailled description of required data:
Reinert, C.; Schellhas, L.; Mannhardt, J.; Shu, D.; Kämper, A.; Baumgärtner, N.; Deutz, S., and Bardow, A. (2022): "SecMOD: An open-source modular framework
combining multi-sector system optimization and life-cycle assessment". Frontiers in Energy Research. DOI: 10.3389/fenrg.2022.884525 .

Input data
==================================
Input data is used to model a given multi-sector system with the desired spatial, technical and temporal resolution. Input data can be added manually in the sampledata folder or in some cases be obtained automatically from open webpages, such as the open power system database by the scripts in sampledata/00-EXTERNAL.

.. image:: ../img/secmod_scheme.svg

Units in SecMOD
==================================
SecMOD employs unitizing to automatically convert units. All units used in the model must be defined in the sample data file.

Output data and evaluation
==================================
The objective of the optimization is to determine an optimal design and operation of the multi-sector system according to the given objective and constraints. 
The output data of the optimization is saved in the working directory in `01-MODEL-RESULTS/InvestmentModel_year.pickle`. 
Usually, the evaluation will start automatically. To start the evaluation without the optimization, type::

    python -m secmod.evaluation

This will open a graphical user interface (GUI). 
In the user interface, the capacity, product flows, and impacts can be shown for all products in different plot types. Clicking on a capacity will open a second layer to investigate the construction years. Further, all raw results are shown as a table next to the plot.
The data can be exported in several formats including tikz, png, xlsx and pdf.


