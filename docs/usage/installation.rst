============
Installation
============

The installation of SecMOD is easily done.
If you want to edit its source code, you need to
follow :ref:`editable-installation`.
If you just want to use the published package,
follow :ref:`package-installation`.


.. _editable-installation:

Editable Installation
=====================

To edit the source code and therefore contribute to the development of SecMOD,
you need to clone the `repository <https://git.rwth-aachen.de/ltt/secmod/secmod>`_
to your local machine **recursively**. Using SSH as authentication to the git server
the following command needs to be used::

    git clone --recurse-submodules -j8 git@git-ce.rwth-aachen.de:ltt/opt/secmod/secmod.git 01-SecMOD

.. NOTE:: If you don´t know yet how to clone a repository have a
    look `here <https://git.rwth-aachen.de/help/#new-to-git-and-gitlab>`_.
    Make sure that you have Python already installed on your PC.

After the clone process is completed, open a python terminal e.g. in your 
IDE (Visual Studio Code, Spyder, ...) or the Anaconda prompt.
Afterwards you can install SecMOD in form of your local repository by using the
following install command::

    pip install --user -e PATH

.. NOTE:: The PATH at the end of the command stands for the directory
    of your local repository. You can either use an absolute path or
    a path relative to the currently active directory of the terminal.

Your local repository should now be installed, if no errors occurred during the process.
You can now proceed to the :doc:`quickstart <quickstart>` guide to complete your setup. 

.. _package-installation:

Package Installation
====================

.. error:: SecMOD is not yet packaged and therefore not available through PyPI.
    Use the :ref:`editable-installation` instead.

    See `Issue 13 <https://git.rwth-aachen.de/ltt/secmod/secmod/issues/13>`_
    for further information about the packaged distribution of SecMOD.

If you don´t need to edit the source code of the SecMOD package,
but just want to use the modules, you can install the package directly
from PyPi using the following command::

    pip install secmod

SecMOD should now be installed, if no errors occurred during the process.
You can now proceed to the :doc:`quickstart <quickstart>` guide to complete your setup. 
