===================
Developer reference
===================


secmod.classes module
=====================

.. automodule:: secmod.classes
    :members:
    :undoc-members:
    :show-inheritance:

secmod.data\_preprocessing module
=================================

.. automodule:: secmod.data_preprocessing
    :members:
    :undoc-members:
    :show-inheritance:

secmod.data\_processing module
==============================

.. automodule:: secmod.data_processing
    :members:
    :undoc-members:
    :show-inheritance:

secmod.evaluation module
========================

.. automodule:: secmod.evaluation
    :members:
    :undoc-members:
    :show-inheritance:

secmod.helpers module
=====================

.. automodule:: secmod.helpers
    :members:
    :undoc-members:
    :show-inheritance:

secmod.optimization module
==========================

.. automodule:: secmod.optimization
    :members:
    :undoc-members:
    :show-inheritance:

secmod.setup module
===================

.. automodule:: secmod.setup
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
===============

.. automodule:: secmod
    :members:
    :undoc-members:
    :show-inheritance:
