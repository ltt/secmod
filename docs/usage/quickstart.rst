==========
Quickstart
==========

In order to improve the distributability of SecMOD the package code is
separated from the handled input data. This allows to use a single installation
of SecMOD for several projects or scenarios in multiple working directories.

To get started with SecMOD, the following two steps must be completed:

1. :doc:`Installation of SecMOD <installation>`
2. :ref:`Set up a working directory <setupWD>`

For the first step follow the instructions for the :doc:`installation of SecMOD <installation>`.
Afterwards return to :ref:`Step 2 <setupWD>` to set up your first working directory.

.. _setupWD:

Set up a working directory
==========================

Create a new directory where you want. Note that this directory will contain all
input and results data and can become large in size, depending on the size of your
examined model.

1. Open a terminal with an activated Python environment (e.g. Anaconda prompt)!
2. Navigate to your newly created directory in the terminal (e.g. "cd D:/WorkingDirectory")!
3. Run the command "python -m secmod.setup" in the terminal!

SecMOD will now create the necessary folders for the input data and furthermore copy the
sample data provided in the SecMOD repository.
Afterwards all necessary data to run SecMOD is available in the working directory. Furthermore,
a file called "start.bat" is created at the top level of your working directory.
This file allows you to start SecMOD with a simple double click. Depending on your Python
installation, you might need to edit the file (right click > edit) according to the instructions.

If you encounter problems during the automatic setup, please refer to :doc:`Troubleshooting <troubleshooting>`.
