import logging
import pickle
import os
import platform
from pathlib import Path
from clint.textui import progress
import pandas as pd
import numpy as np
import pyomo.environ as pe
import pyomo.core.base as pyo_base
import csv
import copy

import secmod.data_processing as dataproc
from secmod.classes import (
Process, Product, config, units, convert_quantity_to_correct_unit)
from secmod.optimization import Optimization
                                
def extract_results(working_directory, debug=False):
    """This method extracts the results of the optimization and dumps them into a pickle file. 

    Args:
        working_directory: Path to the working directory
        debug (bool), default False : specifies whether to debug the file e.g. to extract result for a certain year
    
    Returns:
        None
    """
    if debug == True:
        config.invest_years = [2035]

    logging.info("Load processes")
    optimization_results = {'Results': {}, 'Utilities': {'Unit_dict':{},'Index_names':{},'Unit_time_summation': {}, 'Parameters':{},'ProductsOfProcess':{},'Misc':{}}}
    optimization_results['Utilities']['Parameters'] = {'processes':{},'products':{},'impact_categories':[]}
    # deunitized_input_dictionary = {'Units':{}}
    index_names = {}  
    # define output balances selected from model instance
    list_output_balances =[
                        'non_served_demand', 
                        'new_capacity_production', 
                        'new_capacity_storage', 
                        'new_capacity_transshipment', 
                        'new_capacity_transmission', 
                        'existing_capacity_production', 
                        'existing_capacity_storage', 
                        'existing_capacity_transshipment', 
                        'existing_capacity_transmission', 
                        'demand'
                        ]
    # Load units and Process/Product/Impact names 
    with open(working_directory / "SecMOD" / "00-INPUT" / "01-COMPUTED-INPUT" / "input-{0}.pickle".format(config.invest_years[0]), "rb") as input_file:
        input_dictionary = pickle.load(input_file)

    # Load optimization results
    logging.info("Loading optimization results")
     
    for year in progress.bar(config.invest_years):
    # create abstract optimization model
        # optimization = Optimization()
        with open(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "InvestmentModel_{0}.pickle".format(year), "rb") as input_file:
            # get creation date of Raw Results:"InvestmentModel_"+year[0]+".pickle"
            if year == config.invest_years[0]:
                if platform.system() == 'Windows':
                    date_creation_result_file = os.path.getmtime(input_file.name)
                # different system
                else:
                    date_creation_result_file = os.stat(input_file.name).st_mtime
        # load optimization result instance
            model_instance = pickle.load(input_file)
            
    # iterate through variables in result
        optimization_results, index_names = get_dataframes_from_results(model_instance, optimization_results, index_names, year, list_output_balances)
    
    # apply variable scaling factors to variables
        logging.info("Apply variable scaling factors to variable for year {}".format(str(year)))
        model_instance = overwrite_variable_values(model_instance)

    # calculate product flows for each year
        optimization_results, index_names = calculate_product_flows(model_instance, optimization_results, index_names, input_dictionary, year)

    # calculate impacts for each year
        optimization_results, index_names = calculate_impacts(model_instance, optimization_results, index_names, input_dictionary, year)
    
    ### End of for year in progress.bar(config.invest_years):
    # sum operational impact and invest impact for total impact
    # OVERALL
    index_names['total_impact'] = index_names['operational_impact']
    optimization_results['Results']['total_impact'] = optimization_results['Results']['operational_impact'].add(optimization_results['Results']['invest_impact'], fill_value = 0)
    # PROCESSES
    process_types = ['production','storage','transshipment','transmission']
    for process_type in process_types:
        index_names['total_impact_'+process_type] = index_names['operational_impact_'+process_type]
        optimization_results['Results']['total_impact_'+process_type] = optimization_results['Results']['operational_impact_'+process_type].add(optimization_results['Results']['invest_impact_'+process_type], fill_value = 0)
    # NON SERVED DEMAND
    index_names['total_impact_non_served_demand'] = index_names['operational_impact_non_served_demand']
    optimization_results['Results']['total_impact_non_served_demand'] = optimization_results['Results']['operational_impact_non_served_demand'].add(optimization_results['Results']['invest_impact_non_served_demand'], fill_value = 0)
    
    # iterate through sets in result for last year to get names of parameters
    for balance_in_optimization in model_instance.component_objects(pe.Set,active=True):
        if 'processes' in balance_in_optimization.name: 
            optimization_results['Utilities']['Parameters']['processes'][balance_in_optimization.name] = balance_in_optimization.ordered_data()
        if 'products' == balance_in_optimization.name: 
            optimization_results['Utilities']['Parameters'][balance_in_optimization.name] = balance_in_optimization.ordered_data()
        if 'impact_categories' == balance_in_optimization.name: 
            optimization_results['Utilities']['Parameters']['impact_categories'] = balance_in_optimization.ordered_data()
    
    # get processes corresponding to products
    optimization_results = get_processes_of_products(optimization_results,input_dictionary)
    
    # set index names
    for balance_in_optimization in optimization_results['Results']:
        optimization_results['Results'][balance_in_optimization].index.names = index_names[balance_in_optimization]

    # add new capacity to existing capacity: existing_capacity_x(node, construction_year, year) += new_capacity_x(node,year) 
    for process_category in progress.bar(optimization_results['Utilities']['Parameters']['processes'].keys()):
        category = process_category.replace('processes','')
        for year in config.invest_years:
            # ATTENTION index of construction years explicitly used
            optimization_results['Results']['existing_capacity'+category].loc[(slice(None),slice(None),year),year]+=optimization_results['Results']['new_capacity'+category][year]
    logging.info('New capacities added to existing capacities')

    #remove values that are smaller than a threshold value (numerical error)
    for key, result in optimization_results['Results'].items():
        result[(result<10**(-10)) & (result>-10**(-10))] = 0

    ## get units
    # get unit of capacities
    for process_type in optimization_results['Utilities']['Parameters']['processes']:
        for process in optimization_results['Utilities']['Parameters']['processes'][process_type]:
            unit = input_dictionary["target_units"]["processes"][process]
            optimization_results['Utilities']['Unit_dict'][process] = unit
    # get unit of products
    for product in optimization_results['Utilities']['Parameters']['products']:
        unit = input_dictionary["target_units"]["products"][product]
        optimization_results['Utilities']['Unit_dict'][product] = unit
    # get unit of impacts
    for impact in optimization_results['Utilities']['Parameters']['impact_categories']:
        unit = input_dictionary["target_units"]["impacts"][impact]
        optimization_results['Utilities']['Unit_dict'][impact] = unit

    # group balance by time slice and nodes/connections/construction years
    # and copy as new balance
    results_temp = {}
    for balance_in_optimization in progress.bar(optimization_results['Results'].keys()):
        # group by time slice and return balance, unit_time_summation appendix and index name
        optimization_results['Results'][balance_in_optimization], optimization_results['Utilities']['Unit_time_summation'][balance_in_optimization], index_names[balance_in_optimization] = \
            group_by_time_slice(optimization_results['Results'][balance_in_optimization], balance_in_optimization, index_names[balance_in_optimization], model_instance.time_slice_yearly_weight,optimization_results['Utilities']['Unit_dict'])
        # copy balance and group by nodes/connections and construction year
        # copy index_names and unit_time_summation
        results_temp[balance_in_optimization+'_grouped'],index_names[balance_in_optimization+'_grouped'] = \
            group_by_lower_index(optimization_results['Results'][balance_in_optimization],index_names[balance_in_optimization])
        optimization_results['Utilities']['Unit_time_summation'][balance_in_optimization +'_grouped'] = optimization_results['Utilities']['Unit_time_summation'][balance_in_optimization]
    # update optimization_results['Results']
    optimization_results['Results'].update(results_temp)

    # copy index_names and time slice yearly weights for export 
    optimization_results['Utilities']['Index_names']= index_names
    optimization_results['Utilities']['Misc']['time_slice_yearly_weights'] = model_instance.time_slice_yearly_weight
    
    logging.info('Results simplified and unitized')
    # create Result folder
    if not os.path.exists(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "Extracted Results"):
                os.mkdir(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "Extracted Results")

    # save optimization results
    with open(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "Extracted Results" / "Optimization_Results.pickle", 'wb') as output_file:
        pickle.dump(optimization_results, output_file)
        logging.info('Results saved')

    # save date log file
    with open(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "Extracted Results" / "Date_Log_Raw_Results.pickle", 'wb') as output_file:
        pickle.dump(date_creation_result_file, output_file)

def get_dataframes_from_results(model_instance, optimization_results, index_names, year, list_output_balances):
    """This method retrieves dataframes with parameters and variables from. results
    Args:
        model_instance: optimization result instance
        optimization_results(dict): dictionary to write the results to
        index_names(dict): dictionary to write the index names to 
        year: the year in which the results are evaluated
        list_output_balances(list): list that defines output balances selected from model instance 
    
    Returns:
        optimization_results(dict): dictionary filled with results	
	    index_names(dict): dictionary of the index names of the optimization_results dictionary 
    """         
    
    logging.info("Loading optimization variable for year {}".format(str(year)))
    for balance_in_optimization_name in progress.bar(list_output_balances):
        balance_in_optimization = getattr(model_instance,balance_in_optimization_name)
        # get index of 'year' index and list of index_names
        index_names[balance_in_optimization_name], idxyear = get_list_of_index_names(balance_in_optimization)
        if isinstance(balance_in_optimization, pyo_base.var.Var): # if variable
            variable_scaling_factors = getattr(model_instance,'scaling_'+balance_in_optimization.name)
            if balance_in_optimization_name not in optimization_results['Results']: 
                # get results for first year
                optimization_results['Results'][balance_in_optimization_name] = pd.DataFrame(Optimization().get_dataframe_from_result(balance_in_optimization,variable_scaling_factors, idxyear)[year])
            else:
                # append results for subsequent years
                optimization_results['Results'][balance_in_optimization_name] = optimization_results['Results'][balance_in_optimization_name].join(pd.DataFrame(Optimization().get_dataframe_from_result(balance_in_optimization,variable_scaling_factors, idxyear)[year]),how='outer') 
        elif isinstance(balance_in_optimization, pyo_base.param.Param): # if parameter
            if balance_in_optimization_name not in optimization_results['Results']: 
                # get results for first year
                optimization_results['Results'][balance_in_optimization_name] = pd.DataFrame(Optimization().get_dataframe_from_parameter(balance_in_optimization, idxyear)[year])
            else:
                # append results for subsequent years
                optimization_results['Results'][balance_in_optimization_name] = optimization_results['Results'][balance_in_optimization_name].join(pd.DataFrame(Optimization().get_dataframe_from_parameter(balance_in_optimization, idxyear)[year]),how='outer') 
        optimization_results['Results'][balance_in_optimization_name].fillna(0,inplace=True)
        optimization_results['Results'][balance_in_optimization_name].columns.names = ['invest_year']
    return optimization_results, index_names

def calculate_product_flows(model_instance, optimization_results, index_names, input_dictionary, year):
    """This method calculates product flows.
    Args:
        model_instance: optimization result instance
        optimization_results(dict): dictionary to write the results to
        index_names(dict): dictionary to write the index names to 
        year: the year in which the results are evaluated
    
    Returns:
        optimization_results(dict): dictionary filled with results	
	    index_names(dict): dictionary of the index names 
    """         
    # calculate PRODUCT FLOWS for production, storage, transmission and transshipment and total
    logging.info("Calculate product flows for year {}".format(str(year)))
    # set up Dataframe [products, nodes, time_slices]/[products,product_process, nodes, time_slices]
    if year == config.invest_years[0]:
        # set index of total product flow
        index_names['product_flow'] = ['products','nodes','time_slices']
        # get product, nodes and time slices of product flow
        index_product_flow = [model_instance.products.ordered_data(),model_instance.nodes.ordered_data(),model_instance.time_slices.ordered_data()]
        # create multiindex of total product flow
        index_product_flow = pd.MultiIndex.from_product(index_product_flow, names = index_names['product_flow'])
        # create dataframe of total product flow
        optimization_results['Results']['product_flow'] = pd.DataFrame(index = index_product_flow,columns=config.invest_years)
        optimization_results['Results']['product_flow'].columns.names = ['invest_year']
        # analogously create empty dataframe for process types without transmission
        process_types = ['production','storage','transshipment']
        # set index of product flow
        index_names_product_flow_processes = ['products','product_process','nodes','time_slices']
        for process_type in process_types:
            # get technologymatrix/efficiency matrix for process type
            if process_type == 'production':
                technology_matrix = input_dictionary['technology_matrix_production']
            elif process_type == 'storage':
                technology_matrix = input_dictionary['efficiency_matrix_storage']
            # elif process_type == 'transshipment':
            #     technology_matrix = input_dictionary['transshipment_products']
            # get product, process, nodes and time slices of product flow
            index_product_flow = pd.MultiIndex.from_product([model_instance.products.ordered_data(),eval('model_instance.processes_'+process_type+'.ordered_data()')])
            index_nodes_time_slices = pd.MultiIndex.from_product([model_instance.nodes.ordered_data(),model_instance.time_slices.ordered_data()])
            # get indizes where technologymatrix != 0 (exclude transshipment for now)
            if process_type != 'transshipment':
                if process_type == 'production':
                    _boolean_technologymatrix = [technology_matrix[row+(year,)]!=0 for row in index_product_flow]
                elif process_type == 'storage':
                    _boolean_technologymatrix = [technology_matrix[row+('deposit',year)]!=0 for row in index_product_flow]
                index_product_flow = index_product_flow[_boolean_technologymatrix]
            index_names['product_flow_'+process_type] = index_names_product_flow_processes
            # index_product_flow = pd.MultiIndex.from_product(index_product_flow, names = index_names['product_flow_'+process_type])
            # create dataframe of total product flow
            optimization_results['Results']['product_flow_'+ process_type] = pd.DataFrame(data = 0, index = index_product_flow,columns=index_nodes_time_slices).stack([0,1]).to_frame().drop(0,axis=1)
            optimization_results['Results']['product_flow_'+ process_type].index.names = index_names['product_flow_'+process_type] 
            optimization_results['Results']['product_flow_'+ process_type].columns.names = ['invest_year']
        # copy of dataframe(product_flow) for transmission
        optimization_results['Results']['product_flow_transmission'] = copy.deepcopy(optimization_results['Results']['product_flow'])
        index_names['product_flow_transmission'] = copy.deepcopy(index_names['product_flow'])
    
    # calculate Product flows
    # production processes
    optimization_results['Results']['product_flow_production'][year] = list(map(lambda x: get_product_flow_production(model_instance,year,x),
        optimization_results['Results']['product_flow_production'].index.values))
    # storage processes
    optimization_results['Results']['product_flow_storage'][year] = list(map(lambda x: get_product_flow_storage(model_instance,year,x),
        optimization_results['Results']['product_flow_storage'].index.values))
    # transshipment processes
    optimization_results['Results']['product_flow_transshipment'][year] = list(map(lambda x: get_product_flow_transshipment(model_instance,year,x),
        optimization_results['Results']['product_flow_transshipment'].index.values))
    # transmission processes
    optimization_results['Results']['product_flow_transmission'][year] = list(map(lambda x: get_product_flow_transmission(model_instance,year,x),
        optimization_results['Results']['product_flow_transmission'].index.values))
    # total
    optimization_results['Results']['product_flow'][year] = optimization_results['Results']['product_flow_production'][year].sum(level=[0,2,3]).add(
        optimization_results['Results']['product_flow_storage'][year].sum(level=[0,2,3]), fill_value = 0).add(
        optimization_results['Results']['product_flow_transshipment'][year].sum(level=[0,2,3]),fill_value = 0).add(
        optimization_results['Results']['product_flow_transmission'][year], fill_value = 0)
    
    # return optimization_results and index_names
    return optimization_results, index_names

def calculate_impacts(model_instance, optimization_results, index_names, input_dictionary, year):
    """ calculates impacts
    Args:
        model_instance: optimization result instance
        optimization_results (dict): dictionary to write the results to
        index_names (dict): dictionary to write the index names to 
        year: the year in which the results are evaluated
    
    Returns:
        optimization_results (dict): dictionary filled with results	
	    index_names (dict): dictionary of the index names 
    """
    # calculate IMPACTS for production, storage, transmission and transshipment and total
    logging.info("Calculate impacts for year {}".format(str(year)))
    # set up Dataframe [impact_category, nodes, time_slices]/[impact_category, impact_process/impact_product, nodes, time_slices]
    if year == config.invest_years[0]:
        # for OVERALL
        # set index of impacts
        index_names['operational_impact'] = ['impact_category','nodes','time_slices']
        index_names['invest_impact'] = ['impact_category','nodes','time_slices']
        # get product, nodes and time slices of impacts
        index_impact = [model_instance.impact_categories.ordered_data(),
            model_instance.nodes.ordered_data(),
            model_instance.time_slices.ordered_data()]
        # create multiindex of impacts
        index_impact = pd.MultiIndex.from_product(index_impact, names = index_names['operational_impact'])
        # create dataframe of impacts
        optimization_results['Results']['operational_impact'] = pd.DataFrame(index = index_impact,columns=config.invest_years)
        optimization_results['Results']['operational_impact'].columns.names = ['invest_year']
        optimization_results['Results']['invest_impact'] = copy.deepcopy(optimization_results['Results']['operational_impact'])

        # for PROCESSES
        # analogously create empty dataframe for process types 
        process_types = ['production','storage','transshipment','transmission']
        # set index of impact
        index_names_impacts_processes = ['impact_category','impact_process','nodes','time_slices']
        for process_type in process_types:
            # get impact category, process, nodes and time slices of product flow
            index_impact = [model_instance.impact_categories.ordered_data(),eval('model_instance.processes_'+process_type+'.ordered_data()'), model_instance.nodes.ordered_data(),model_instance.time_slices.ordered_data()]
            # create multiindex of product flow
            index_names['operational_impact_'+process_type] = index_names_impacts_processes
            index_names['invest_impact_'+process_type] = index_names_impacts_processes
            index_impact = pd.MultiIndex.from_product(index_impact, names = index_names['operational_impact_'+process_type])
            # create dataframe of total product flow
            optimization_results['Results']['operational_impact_'+ process_type] = pd.DataFrame(index = index_impact,columns=config.invest_years)
            optimization_results['Results']['operational_impact_'+ process_type].columns.names = ['invest_year']
            optimization_results['Results']['invest_impact_'+ process_type] = copy.deepcopy(optimization_results['Results']['operational_impact_'+ process_type])

        # for NON_SERVED_DEMAND
        # analogously create empty dataframe for non served demand
        demand_type = 'non_served_demand'
        # set index of impact
        index_names_impacts_demand = ['impact_category','impact_products','nodes','time_slices']
        # get impact category, process, nodes and time slices of product flow
        index_impact = [model_instance.impact_categories.ordered_data(),model_instance.products.ordered_data(), model_instance.nodes.ordered_data(),model_instance.time_slices.ordered_data()]
        # create multiindex of product flow
        index_names['operational_impact_'+demand_type] = index_names_impacts_demand
        index_names['invest_impact_'+demand_type] = index_names_impacts_demand
        index_impact = pd.MultiIndex.from_product(index_impact, names = index_names['operational_impact_'+demand_type])
        # create dataframe of total product flow
        optimization_results['Results']['operational_impact_'+ demand_type] = pd.DataFrame(index = index_impact,columns=config.invest_years)
        optimization_results['Results']['operational_impact_'+ demand_type].columns.names = ['invest_year']
        optimization_results['Results']['invest_impact_'+ demand_type] = copy.deepcopy(optimization_results['Results']['operational_impact_'+ demand_type])

    # calculate Impacts
    # production processes
    optimization_results['Results']['operational_impact_production'][year] = list(map(lambda x: get_operational_impact_production(model_instance,year,x),
        optimization_results['Results']['operational_impact_production'].index.values))
    optimization_results['Results']['invest_impact_production'][year] = list(map(lambda x: get_invest_impact_production(model_instance,year,x),
        optimization_results['Results']['invest_impact_production'].index.values))
    # storage processes
    optimization_results['Results']['operational_impact_storage'][year] = list(map(lambda x: get_operational_impact_storage(model_instance,year,x),
        optimization_results['Results']['operational_impact_storage'].index.values))
    optimization_results['Results']['invest_impact_storage'][year] = list(map(lambda x: get_invest_impact_storage(model_instance,year,x),
        optimization_results['Results']['invest_impact_storage'].index.values))
    # transshipment processes
    optimization_results['Results']['operational_impact_transshipment'][year] = list(map(lambda x: get_operational_impact_transshipment(model_instance,year,x),
        optimization_results['Results']['operational_impact_transshipment'].index.values))
    optimization_results['Results']['invest_impact_transshipment'][year] = list(map(lambda x: get_invest_impact_transshipment(model_instance,year,x),
        optimization_results['Results']['invest_impact_transshipment'].index.values))
    # transmission processes
    optimization_results['Results']['operational_impact_transmission'][year] = list(map(lambda x: get_operational_impact_transmission(model_instance,year,x),
        optimization_results['Results']['operational_impact_transmission'].index.values))
    optimization_results['Results']['invest_impact_transmission'][year] = list(map(lambda x: get_invest_impact_transmission(model_instance,year,x),
        optimization_results['Results']['invest_impact_transmission'].index.values))
    # non served demand
    optimization_results['Results']['operational_impact_non_served_demand'][year] = list(map(lambda x: get_operational_impact_non_served_demand(model_instance,year,x),
        optimization_results['Results']['operational_impact_non_served_demand'].index.values))
    optimization_results['Results']['invest_impact_non_served_demand'][year] = list(map(lambda x: get_invest_impact_non_served_demand(model_instance,year,x),
        optimization_results['Results']['invest_impact_non_served_demand'].index.values))
    # overall
    optimization_results['Results']['operational_impact'][year] = (
        optimization_results['Results']['operational_impact_production'][year].sum(level=[0,2,3])
        + optimization_results['Results']['operational_impact_storage'][year].sum(level=[0,2,3])
        + optimization_results['Results']['operational_impact_transshipment'][year].sum(level=[0,2,3])
        + optimization_results['Results']['operational_impact_transmission'][year].sum(level=[0,2,3])
        + optimization_results['Results']['operational_impact_non_served_demand'][year].sum(level=[0,2,3])
    )
    optimization_results['Results']['invest_impact'][year] = (
        optimization_results['Results']['invest_impact_production'][year].sum(level=[0,2,3])
        + optimization_results['Results']['invest_impact_storage'][year].sum(level=[0,2,3])
        + optimization_results['Results']['invest_impact_transshipment'][year].sum(level=[0,2,3])
        + optimization_results['Results']['invest_impact_transmission'][year].sum(level=[0,2,3])
        + optimization_results['Results']['invest_impact_non_served_demand'][year].sum(level=[0,2,3])
    )
    return optimization_results, index_names

def overwrite_variable_values(model_instance):
    """This method overwrites variable values of unscaled model instance with rescaled optimized variable values
    Args:
        model_instance: optimization result instance
    
    Returns:
        model_instance: optimization result instance with rescaled values
    """         
    for variable in model_instance.component_objects(pe.Var, active=True):
        scaling_factor = getattr(model_instance,'scaling_'+variable.name)
        for data in variable:
            if variable[data].value:
                variable[data].value *= scaling_factor[data].value
    return model_instance

def get_list_of_index_names(balance_in_optimization):
    """ get index of 'years' in matrix """
    index_names= []
    for [idxcount,index_of_var] in enumerate(balance_in_optimization._index.subsets()):
        if index_of_var.name == 'years':
            idxyear = idxcount
        else:
            # create list of index names with processes substituted by 'process', and 'construction_years_x' substituted by 'construction_years', in  order to identify level in multiindex
            if index_of_var.name in ['processes_production','processes_storage','processes_transmission','processes_transshipment']:
                index_names.append('process')
            elif 'construction_years' in index_of_var.name:
                index_names.append('construction_years')
            else:
                index_names.append(index_of_var.name)
    return index_names, idxyear

def get_processes_of_products(optimization_results,input_dictionary):
    """ get process corresponding to products """
    optimization_results['Utilities']['ProductsOfProcess']['production_products'] = {}
    optimization_results['Utilities']['ProductsOfProcess']['storage_products'] = {}
    optimization_results['Utilities']['ProductsOfProcess']['transmission_products'] = {}
    optimization_results['Utilities']['ProductsOfProcess']['transshipment_products'] = {}
    for product in optimization_results['Utilities']['Parameters']['products']:
        optimization_results['Utilities']['ProductsOfProcess']['production_products'][product] = []
        optimization_results['Utilities']['ProductsOfProcess']['storage_products'][product] = []
        optimization_results['Utilities']['ProductsOfProcess']['transmission_products'][product] = []
        optimization_results['Utilities']['ProductsOfProcess']['transshipment_products'][product] = []
        # get production processes
        for production_process in optimization_results['Utilities']['Parameters']['processes']['processes_production']:
            if input_dictionary['technology_matrix_production'][(product, production_process, input_dictionary['years'][None][0])] != 0:
                optimization_results['Utilities']['ProductsOfProcess']['production_products'][product].append(production_process)
        # get storage processes
        for storage_process in optimization_results['Utilities']['Parameters']['processes']['processes_storage']:
            if input_dictionary['storage_products'][storage_process] == product:
                optimization_results['Utilities']['ProductsOfProcess']['storage_products'][product].append(storage_process)
        # get storage processes
        for transmission_process in optimization_results['Utilities']['Parameters']['processes']['processes_transmission']:
            if product in input_dictionary['products_transmission'][None]:
                optimization_results['Utilities']['ProductsOfProcess']['transmission_products'][product].append(transmission_process)
        # get storage processes
        for transshipment_process in optimization_results['Utilities']['Parameters']['processes']['processes_transshipment']:
            if input_dictionary['transshipment_products'][transshipment_process] == product:
                optimization_results['Utilities']['ProductsOfProcess']['transshipment_products'][product].append(transshipment_process)
    return optimization_results

def group_by_time_slice(balance_in_optimization,balance_name, index_name, time_slice_weight, unit_dict):
    """ group balance by time slices. Eliminates time slice index from dataframe """
    unit_time_summation = ''
    if 'time_slices' in index_name and len(index_name)>1:
        # unit of impacts already integrated and multiplied with time slice weights
        if 'impact' not in balance_name:
            # add time slice weight list
            list_time_slice_weight = []
            # get weights of time slices for row in effect
            for row in balance_in_optimization.index.get_level_values('time_slices'):
                list_time_slice_weight.append(time_slice_weight[row])
            # multiply each column with time slice weight
            balance_in_optimization = balance_in_optimization.apply(lambda column: column * list_time_slice_weight)
            # add unit_time_summation
            unit_time_summation = '*hour'
            # get units
            if 'products' in balance_in_optimization.index.names:
                unit_list = [unit_dict[row]+unit_time_summation for row in balance_in_optimization.index.get_level_values('products')]
            elif 'process' in balance_in_optimization.index.names:
                unit_list = [unit_dict[row]+unit_time_summation for row in balance_in_optimization.index.get_level_values('process')]
            # append units to dataframe
            balance_in_optimization['unit']=unit_list
            # convert dataframe to correct unit
            balance_in_optimization = convert_quantity_to_correct_unit(balance_in_optimization)
        index_name = [index_of_var for index_of_var in index_name if index_of_var != 'time_slices']
        balance_in_optimization = balance_in_optimization.groupby(index_name).sum() # sum over time slices
    return balance_in_optimization, unit_time_summation, index_name

def group_by_lower_index(balance_in_optimization,index_name):
    """ group balance by nodes/connections and construction years. Eliminates indices from dataframe """
    # sum over nodes, if 'nodes' in index
    if 'nodes' in index_name and len(index_name)>1:
        index_name = [index_of_var for index_of_var in index_name if index_of_var != 'nodes']
        balance_in_optimization = balance_in_optimization.groupby(index_name).sum()
    # sum over connections, if 'connections' in index
    if 'connections' in index_name and len(index_name)>1:
        index_name = [index_of_var for index_of_var in index_name if index_of_var != 'connections']
        balance_in_optimization = balance_in_optimization.groupby(index_name).sum()
    # sum over construction years, if 'construction_years' in index
    if 'construction_years' in index_name and len(index_name)>1:
        index_name = [index_of_var for index_of_var in index_name if index_of_var != 'construction_years']
        balance_in_optimization = balance_in_optimization.groupby(index_name).sum()
    return balance_in_optimization, index_name

def get_product_flow_production(model, year: int, multiindex_entry):
    """  returns the product flow for PRODUCTION at a node in a specific year and time slice equal to the sum of all process contributions to that product balance 
    identical structure as constraint_product_balance_rule in secmod.optimization"""
    product = multiindex_entry[0]
    process_production = multiindex_entry[1]
    node = multiindex_entry[2]
    time_slice = multiindex_entry[3]
    return (
        # sum over all construction years to differentiate between the used capacities
        sum(model.technology_matrix_production[product, process_production, year_construction]
            * model.used_production[node, process_production, year, year_construction, time_slice].value
            for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_production[process_production, year_construction]), model.construction_years_production))
        )

def get_product_flow_storage(model, year: int, multiindex_entry):
    """  returns the product flow for STORAGE at a node in a specific year and time slice equal to the sum of all process contributions to that product balance 
    identical structure as constraint_product_balance_rule in secmod.optimization"""
    product = multiindex_entry[0]
    process_storage = multiindex_entry[1]
    node = multiindex_entry[2]
    time_slice = multiindex_entry[3]
    if product == model.storage_products[process_storage]:
        return (
                # sum over all construction years to differentiate between the used capacities
                + sum(
                    sum(model.used_storage[node, process_storage, direction_storage, year, year_construction, time_slice].value
                        * (- model.storage_level_factor[direction_storage])
                        # sum over both storage direction (deposit and withdraw)
                        for direction_storage in model.directions_storage)
                    for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_storage[process_storage, year_construction]), model.construction_years_storage))
        )
    else:
        return 0

def get_product_flow_transshipment(model, year: int, multiindex_entry):      
    """  returns the product flow for TRANSSHIPMENT at a node in a specific year and time slice equal to the sum of all process contributions to that product balance 
    identical structure as constraint_product_balance_rule in secmod.optimization"""
    product = multiindex_entry[0]
    process_transshipment = multiindex_entry[1]
    node = multiindex_entry[2]
    time_slice = multiindex_entry[3]
    if product == model.transshipment_products[process_transshipment]:
        return (
                # sum over all construction years to differentiate between the used capacities
                sum(
                    # Sum of all streams of connections which have this node as first node
                    sum(model.transshipment_efficiency[process_transshipment, connection]
                        * model.used_transshipment[connection, process_transshipment, "backward", year, year_construction, time_slice].value
                        - model.used_transshipment[connection, process_transshipment, "forward", year, year_construction, time_slice].value
                        for connection in filter(lambda connection: model.connected_nodes[connection, "node1"] == node, model.connections))
                    # Sum of all streams of connections, which have this node as second node, therefore multiplied with the efficiency of the connection
                    + sum(model.transshipment_efficiency[process_transshipment, connection]
                        * model.used_transshipment[connection, process_transshipment, "forward", year, year_construction, time_slice].value
                        - model.used_transshipment[connection, process_transshipment, "backward", year, year_construction, time_slice].value
                        for connection in filter(lambda connection: model.connected_nodes[connection, "node2"] == node, model.connections))
                    
                    for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_transshipment[process_transshipment, year_construction]), model.construction_years_transshipment))
        )
    else:
        return 0

def get_product_flow_transmission(model, year: int, multiindex_entry):      
    """  returns the product flow for TRANSMISSION at a node in a specific year and time slice equal to the sum of all process contributions to that product balance 
    identical structure as constraint_product_balance_rule in secmod.optimization"""
    product = multiindex_entry[0]
    node = multiindex_entry[1]
    time_slice = multiindex_entry[2]
    return (
        # adding the power from DC load flow transmission, if the product is electricity
        ( # Sum of the power from all connections which have this node as first node
            sum(
                model.power_line_properties[connection, year, "susceptance per unit"]
                * (model.phase_difference[model.connected_nodes[connection, "node2"], year, time_slice].value
                    - model.phase_difference[node, year, time_slice].value)
                for connection in filter(lambda connection: (model.connected_nodes[connection, "node1"] == node) and (product in model.products_transmission), model.connections))
            # Sum of the power from all connections which have this node as second node
            + sum(
                model.power_line_properties[connection, year, "susceptance per unit"]
                * (model.phase_difference[model.connected_nodes[connection, "node1"], year, time_slice].value
                    - model.phase_difference[node, year, time_slice].value)
                for connection in filter(lambda connection: (model.connected_nodes[connection, "node2"] == node) and (product in model.products_transmission), model.connections))
        ) * model.per_unit_base.value
    )

def get_operational_impact_production(model, year: int, multiindex_entry):
    """  returns the oprerational impact for PRODUCTION at a node in a specific year and time slice equal to the sum of all process contributions to that impact 
    identical structure as constraint_operational_nodal_impact_rule in secmod.optimization"""
    impact_category = multiindex_entry[0]
    process_production = multiindex_entry[1]
    node = multiindex_entry[2]
    time_slice = multiindex_entry[3]
    annuity_correction = 1
    if impact_category == 'cost':
        # if the impact category is cost, we divide by the annuity factor of the investment period:
        # In the optimization we annualize the total cost by determining cost factors in the impact matrix that
        # represent the full investment period (i.e., the present value  of the cost in the full
        # investment period, e.g., the present value of the cost for the investment period 2020-2025). This is necessary
        # for the rolling horizon optimization if we consider foresight with multiple investment periods of different
        # lengths.
        # see `classes.py`: `_get_cost_invest` and `_get_cost_operation` in `ProcessImpacts` class
        # However, in the results we want to know the cost per year (e.g., the total annualized cost in 2020).
        # Therefore, we need to divide the cost by the `investment_period_annuity_present_value_factor`.
        investment_period_duration = Process._get_investment_period_duration(year)
        annuity_correction =    ((1 + Process.interest_rate) ** (investment_period_duration / units.year) - 1) / \
                                ((1 + Process.interest_rate) ** (investment_period_duration / units.year) * Process.interest_rate)
    # sum over the construction years of the production process
    return(
        sum(model.used_production[node, process_production, year, year_construction, time_slice].value
        * model.impact_matrix_production[impact_category, 'operation', process_production, year, year_construction]
        for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_production[process_production, year_construction]), model.construction_years_production))
        # multiply the impact with the weight of the current time slice
        * model.time_slice_yearly_weight[time_slice] / annuity_correction)

def get_operational_impact_storage(model, year: int, multiindex_entry):
    """  returns the oprerational impact for STORAGE at a node in a specific year and time slice equal to the sum of all process contributions to that impact 
    identical structure as constraint_operational_nodal_impact_rule in secmod.optimization"""
    impact_category = multiindex_entry[0]
    process_storage = multiindex_entry[1]
    node = multiindex_entry[2]
    time_slice = multiindex_entry[3]
    annuity_correction = 1
    if impact_category == 'cost':
        # if the impact category is cost, we divide by the annuity factor of the investment period:
        # In the optimization we annualize the total cost by determining cost factors in the impact matrix that
        # represent the full investment period (i.e., the present value  of the cost in the full
        # investment period, e.g., the present value of the cost for the investment period 2020-2025). This is necessary
        # for the rolling horizon optimization if we consider foresight with multiple investment periods of different
        # lengths.
        # see `classes.py`: `_get_cost_invest` and `_get_cost_operation` in `ProcessImpacts` class
        # However, in the results we want to know the cost per year (e.g., the total annualized cost in 2020).
        # Therefore, we need to divide the cost by the `investment_period_annuity_present_value_factor`.
        investment_period_duration = Process._get_investment_period_duration(year)
        annuity_correction =    ((1 + Process.interest_rate) ** (investment_period_duration / units.year) - 1) / \
                                ((1 + Process.interest_rate) ** (investment_period_duration / units.year) * Process.interest_rate)
    # sum over the construction years of the storage process
    return(
        sum(
        # impact is assumed to be the impact per withdrawn unit of stored product
        model.impact_matrix_storage[impact_category, 'operation', process_storage, year, year_construction]
        * model.used_storage[node, process_storage, "withdraw", year, year_construction, time_slice].value
        for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_storage[process_storage, year_construction]), model.construction_years_storage))
        # multiply the impact with the weight of the current time slice
        * model.time_slice_yearly_weight[time_slice] / annuity_correction)

def get_operational_impact_transshipment(model, year: int, multiindex_entry):
    """  returns the oprerational impact for TRANSSHIPMENT at a node in a specific year and time slice equal to the sum of all process contributions to that impact 
    identical structure as constraint_operational_nodal_impact_rule in secmod.optimization"""
    impact_category = multiindex_entry[0]
    process_transshipment = multiindex_entry[1]
    node = multiindex_entry[2]
    time_slice = multiindex_entry[3]
    annuity_correction = 1
    if impact_category == 'cost':
        # if the impact category is cost, we divide by the annuity factor of the investment period:
        # In the optimization we annualize the total cost by determining cost factors in the impact matrix that
        # represent the full investment period (i.e., the present value  of the cost in the full
        # investment period, e.g., the present value of the cost for the investment period 2020-2025). This is necessary
        # for the rolling horizon optimization if we consider foresight with multiple investment periods of different
        # lengths.
        # see `classes.py`: `_get_cost_invest` and `_get_cost_operation` in `ProcessImpacts` class
        # However, in the results we want to know the cost per year (e.g., the total annualized cost in 2020).
        # Therefore, we need to divide the cost by the `investment_period_annuity_present_value_factor`.
        investment_period_duration = Process._get_investment_period_duration(year)
        annuity_correction =    ((1 + Process.interest_rate) ** (investment_period_duration / units.year) - 1) / \
                                ((1 + Process.interest_rate) ** (investment_period_duration / units.year) * Process.interest_rate)
    return(
        # sum over the construction years of the transshipment process
        sum(
            # impact of transshipment is distributed equally (0.5:0.5) between the two connected nodes
            0.5 * model.impact_matrix_transshipment[impact_category, 'operation', process_transshipment, year, year_construction]
            # sum over both transshipment directions
            * sum(
                # sum over all connections, which have this node as first node
                sum(model.used_transshipment[connection, process_transshipment, direction_transshipment, year, year_construction, time_slice].value
                    for connection in filter(lambda connection: model.connected_nodes[connection, "node1"] == node, model.connections))
                # sum over all connections, which have this node as second node
                + sum(model.used_transshipment[connection, process_transshipment, direction_transshipment, year, year_construction, time_slice].value
                    for connection in filter(lambda connection: model.connected_nodes[connection, "node2"] == node, model.connections))
                for direction_transshipment in model.directions_transshipment)
            for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_transshipment[process_transshipment, year_construction]), model.construction_years_transshipment))
        * model.time_slice_yearly_weight[time_slice] / annuity_correction)

def get_operational_impact_transmission(model, year: int, multiindex_entry):
    """  returns the oprerational impact for TRANSMISSION at a node in a specific year and time slice equal to the sum of all process contributions to that impact 
    identical structure as constraint_operational_nodal_impact_rule in secmod.optimization
    Assumed to be negligible
    """
    return 0

def get_operational_impact_non_served_demand(model, year: int, multiindex_entry):
    """  returns the oprerational impact for NON SERVED DEMAND at a node in a specific year and time slice equal to the sum of all process contributions to that impact 
    identical structure as constraint_operational_nodal_impact_rule in secmod.optimization
    """
    impact_category = multiindex_entry[0]
    product = multiindex_entry[1]
    node = multiindex_entry[2]
    time_slice = multiindex_entry[3]
    annuity_correction = 1
    if impact_category == 'cost':
        # if the impact category is cost, we divide by the annuity factor of the investment period:
        # In the optimization we annualize the total cost by determining cost factors in the impact matrix that
        # represent the full investment period (i.e., the present value  of the cost in the full
        # investment period, e.g., the present value of the cost for the investment period 2020-2025). This is necessary
        # for the rolling horizon optimization if we consider foresight with multiple investment periods of different
        # lengths.
        # see `classes.py`: `_get_cost_invest` and `_get_cost_operation` in `ProcessImpacts` class
        # However, in the results we want to know the cost per year (e.g., the total annualized cost in 2020).
        # Therefore, we need to divide the cost by the `investment_period_annuity_present_value_factor`.
        investment_period_duration = Process._get_investment_period_duration(year)
        annuity_correction =    ((1 + Process.interest_rate) ** (investment_period_duration / units.year) - 1) / \
                                ((1 + Process.interest_rate) ** (investment_period_duration / units.year) * Process.interest_rate)
    return(
        model.non_served_demand[node, product, year, time_slice].value
        * model.impact_matrix_non_served_demand[impact_category, product, year, time_slice]
        * model.time_slice_yearly_weight[time_slice] / annuity_correction)

def get_invest_impact_production(model, year: int, multiindex_entry):
    """  returns the invest impact for PRODUCTION at a node in a specific year and time slice equal to the sum of all process contributions to that impact 
    identical structure as constraint_operational_nodal_impact_rule in secmod.optimization"""
    impact_category = multiindex_entry[0]
    process_production = multiindex_entry[1]
    node = multiindex_entry[2]
    time_slice = multiindex_entry[3]
    annuity_correction = 1
    if impact_category == 'cost':
        # if the impact category is cost, we divide by the annuity factor of the investment period:
        # In the optimization we annualize the total cost by determining cost factors in the impact matrix that
        # represent the full investment period (i.e., the present value  of the cost in the full
        # investment period, e.g., the present value of the cost for the investment period 2020-2025). This is necessary
        # for the rolling horizon optimization if we consider foresight with multiple investment periods of different
        # lengths.
        # see `classes.py`: `_get_cost_invest` and `_get_cost_operation` in `ProcessImpacts` class
        # However, in the results we want to know the cost per year (e.g., the total annualized cost in 2020).
        # Therefore, we need to divide the cost by the `investment_period_annuity_present_value_factor`.
        investment_period_duration = Process._get_investment_period_duration(year)
        annuity_correction =    ((1 + Process.interest_rate) ** (investment_period_duration / units.year) - 1) / \
                                ((1 + Process.interest_rate) ** (investment_period_duration / units.year) * Process.interest_rate)
    return(
        # EXISTING CAPACITY
        # sum over the construction years of the production process
        (sum(model.existing_capacity_production[node, process_production, year, year_construction]
            * model.impact_matrix_production[impact_category, 'invest', process_production, year, year_construction]
            for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_production[process_production, year_construction]), model.construction_years_production))
        +
        # NEW CAPACITY
        # sum over the construction years of the production process
        sum(model.new_capacity_production[node, process_production, earlier_year].value
            * model.impact_matrix_production[impact_category, 'invest', process_production, year, earlier_year]
            for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year <= model.lifetime_duration_production[process_production, earlier_year]), model.years)))
        * model.time_slice_yearly_weight[time_slice] / sum(model.time_slice_yearly_weight[time_slice] for time_slice in model.time_slices) / annuity_correction)

def get_invest_impact_storage(model, year: int, multiindex_entry):
    """  returns the invest impact for STORAGE at a node in a specific year and time slice equal to the sum of all process contributions to that impact 
    identical structure as constraint_operational_nodal_impact_rule in secmod.optimization"""
    impact_category = multiindex_entry[0]
    process_storage = multiindex_entry[1]
    node = multiindex_entry[2]
    time_slice = multiindex_entry[3]
    annuity_correction = 1
    if impact_category == 'cost':
        # if the impact category is cost, we divide by the annuity factor of the investment period:
        # In the optimization we annualize the total cost by determining cost factors in the impact matrix that
        # represent the full investment period (i.e., the present value  of the cost in the full
        # investment period, e.g., the present value of the cost for the investment period 2020-2025). This is necessary
        # for the rolling horizon optimization if we consider foresight with multiple investment periods of different
        # lengths.
        # see `classes.py`: `_get_cost_invest` and `_get_cost_operation` in `ProcessImpacts` class
        # However, in the results we want to know the cost per year (e.g., the total annualized cost in 2020).
        # Therefore, we need to divide the cost by the `investment_period_annuity_present_value_factor`.
        investment_period_duration = Process._get_investment_period_duration(year)
        annuity_correction =    ((1 + Process.interest_rate) ** (investment_period_duration / units.year) - 1) / \
                                ((1 + Process.interest_rate) ** (investment_period_duration / units.year) * Process.interest_rate)

    return(
        # EXISTING CAPACITY
        # sum over the construction years of the storage process
        (sum(model.existing_capacity_storage[node, process_storage, year, year_construction]
            * model.impact_matrix_storage[impact_category, 'invest', process_storage, year, year_construction]
            for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_storage[process_storage, year_construction]), model.construction_years_storage))
        +
        # NEW CAPACITY
        # sum over the construction years of the storage process
        sum(model.new_capacity_storage[node, process_storage, earlier_year].value
            * model.impact_matrix_storage[impact_category, 'invest', process_storage, year, earlier_year]
            for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year <= model.lifetime_duration_storage[process_storage, earlier_year]), model.years)))
        * model.time_slice_yearly_weight[time_slice] / sum(model.time_slice_yearly_weight[time_slice] for time_slice in model.time_slices) / annuity_correction)

def get_invest_impact_transshipment(model, year: int, multiindex_entry):
    """  returns the invest impact for TRANSSHIPMENT at a node in a specific year and time slice equal to the sum of all process contributions to that impact 
    identical structure as constraint_operational_nodal_impact_rule in secmod.optimization"""
    impact_category = multiindex_entry[0]
    process_transshipment = multiindex_entry[1]
    node = multiindex_entry[2]
    time_slice = multiindex_entry[3]
    annuity_correction = 1
    if impact_category == 'cost':
        # if the impact category is cost, we divide by the annuity factor of the investment period:
        # In the optimization we annualize the total cost by determining cost factors in the impact matrix that
        # represent the full investment period (i.e., the present value  of the cost in the full
        # investment period, e.g., the present value of the cost for the investment period 2020-2025). This is necessary
        # for the rolling horizon optimization if we consider foresight with multiple investment periods of different
        # lengths.
        # see `classes.py`: `_get_cost_invest` and `_get_cost_operation` in `ProcessImpacts` class
        # However, in the results we want to know the cost per year (e.g., the total annualized cost in 2020).
        # Therefore, we need to divide the cost by the `investment_period_annuity_present_value_factor`.
        investment_period_duration = Process._get_investment_period_duration(year)
        annuity_correction =    ((1 + Process.interest_rate) ** (investment_period_duration / units.year) - 1) / \
                                ((1 + Process.interest_rate) ** (investment_period_duration / units.year) * Process.interest_rate)
    return(
        # EXISTING CAPACITY
        # sum over the construction years of the transshipment process
        (sum(
            # impact distributed equally (0.5:0.5) between the two connected nodes
            0.5 * model.impact_matrix_transshipment[impact_category, 'invest', process_transshipment, year, year_construction]
            # multiplied with the length and capacity of all connections of the node
            * (
                # sum over all connections, which have this node as first node
                sum(model.existing_capacity_transshipment[connection, process_transshipment, year, year_construction]
                    * model.connection_length[connection]
                    for connection in filter(lambda connection: model.connected_nodes[connection, "node1"] == node, model.connections))
                # sum over all connections, which have this node as second node
                + sum(model.existing_capacity_transshipment[connection, process_transshipment, year, year_construction]
                    * model.connection_length[connection]
                    for connection in filter(lambda connection: model.connected_nodes[connection, "node2"] == node, model.connections))
            )
            for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_transshipment[process_transshipment, year_construction]), model.construction_years_transshipment))
        +
        # NEW CAPACITY
        # sum over the construction years of the transshipment process
        sum(
            # impact distributed equally (0.5:0.5) between the two connected nodes
            0.5 * model.impact_matrix_transshipment[impact_category, 'invest', process_transshipment, year, earlier_year]
            # multiplied with the length and capacity of all connections of the node
            * (
                # sum over all connections, which have this node as first node
                sum(model.new_capacity_transshipment[connection, process_transshipment, earlier_year].value
                    * model.connection_length[connection]
                    for connection in filter(lambda connection: model.connected_nodes[connection, "node1"] == node, model.connections))
                # sum over all connections, which have this node as second node
                + sum(model.new_capacity_transshipment[connection, process_transshipment, earlier_year].value
                    * model.connection_length[connection]
                    for connection in filter(lambda connection: model.connected_nodes[connection, "node2"] == node, model.connections))
            )
            for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year <= model.lifetime_duration_transshipment[process_transshipment, earlier_year]), model.years)))
        * model.time_slice_yearly_weight[time_slice] / sum(model.time_slice_yearly_weight[time_slice] for time_slice in model.time_slices) / annuity_correction)

def get_invest_impact_transmission(model, year: int, multiindex_entry):
    """  returns the invest impact for TRANSMISSION at a node in a specific year and time slice equal to the sum of all process contributions to that impact 
    identical structure as constraint_operational_nodal_impact_rule in secmod.optimization"""
    impact_category = multiindex_entry[0]
    process_transmission = multiindex_entry[1]
    node = multiindex_entry[2]
    time_slice = multiindex_entry[3]
    annuity_correction = 1
    if impact_category == 'cost':
        # if the impact category is cost, we divide by the annuity factor of the investment period:
        # In the optimization we annualize the total cost by determining cost factors in the impact matrix that
        # represent the full investment period (i.e., the present value  of the cost in the full
        # investment period, e.g., the present value of the cost for the investment period 2020-2025). This is necessary
        # for the rolling horizon optimization if we consider foresight with multiple investment periods of different
        # lengths.
        # see `classes.py`: `_get_cost_invest` and `_get_cost_operation` in `ProcessImpacts` class
        # However, in the results we want to know the cost per year (e.g., the total annualized cost in 2020).
        # Therefore, we need to divide the cost by the `investment_period_annuity_present_value_factor`.
        investment_period_duration = Process._get_investment_period_duration(year)
        annuity_correction =    ((1 + Process.interest_rate) ** (investment_period_duration / units.year) - 1) / \
                                ((1 + Process.interest_rate) ** (investment_period_duration / units.year) * Process.interest_rate)
    return(
        # EXISTING CAPACITY
        # sum over the construction years of the transmission process
        (sum(
            # impact distributed equally (0.5:0.5) between the two connected nodes
            0.5 * model.impact_matrix_transmission[impact_category, 'invest', process_transmission, year, year_construction] 
            # multiplied with the length and power capacity of all connections of the node
            * (
                # sum over all connections, which have this node as first node
                sum(model.existing_capacity_transmission[connection, process_transmission, year, year_construction]
                    * model.power_limit_per_circuit[process_transmission]
                    * model.connection_length[connection]
                    for connection in filter(lambda connection: model.connected_nodes[connection, "node1"] == node, model.connections))
                # sum over all connections, which have this node as second node
                + sum(model.existing_capacity_transmission[connection, process_transmission, year, year_construction]
                    * model.power_limit_per_circuit[process_transmission]
                    * model.connection_length[connection]
                    for connection in filter(lambda connection: model.connected_nodes[connection, "node2"] == node, model.connections))
            )
            for year_construction in filter(lambda year_construction: (year_construction <= year) and (year - year_construction < model.lifetime_duration_transmission[process_transmission, year_construction]), model.construction_years_transmission))
        +
        # NEW CAPACITY
        # sum over the construction years of the transmission process
        sum(
            # impact distributed equally (0.5:0.5) between the two connected nodes
            0.5 * model.impact_matrix_transmission[impact_category, 'invest', process_transmission, year, earlier_year] 
            # multiplied with the length and power capacity of all connections of the node
            * (
                # sum over all connections, which have this node as first node
                sum(model.new_capacity_transmission[connection, process_transmission, earlier_year].value
                    * model.power_limit_per_circuit[process_transmission]
                    * model.connection_length[connection]
                    for connection in filter(lambda connection: model.connected_nodes[connection, "node1"] == node, model.connections))
                # sum over all connections, which have this node as second node
                + sum(model.new_capacity_transmission[connection, process_transmission, earlier_year].value
                    * model.power_limit_per_circuit[process_transmission]
                    * model.connection_length[connection]
                    for connection in filter(lambda connection: model.connected_nodes[connection, "node2"] == node, model.connections))
            )
            for earlier_year in filter(lambda earlier_year: (earlier_year <= year) and (year - earlier_year <= model.lifetime_duration_transmission[process_transmission, earlier_year]), model.years)))
        * model.time_slice_yearly_weight[time_slice] / sum(model.time_slice_yearly_weight[time_slice] for time_slice in model.time_slices) / annuity_correction)

def get_invest_impact_non_served_demand(model, year: int, multiindex_entry):
    """  returns the oprerational impact for NON SERVED DEMAND at a node in a specific year and time slice equal to the sum of all process contributions to that impact 
    identical structure as constraint_invest_nodal_impact_rule in secmod.optimization
    Obviously 0, since no invest necessary for non served demand
    """         
    return 0

if __name__ == "__main__":
    log_format = '%(asctime)s %(filename)s: %(levelname)s: %(message)s'
    if not os.path.exists("logs"):
                os.mkdir("logs")
    logging.basicConfig(filename='logs/secmod.log', level=logging.INFO, format=log_format,
                        datefmt='%Y-%m-%d %H:%M:%S')
    logging.getLogger().addHandler(logging.StreamHandler())
    working_directory = Path().cwd()
    extract_results(working_directory, debug=False)

    logging.getLogger().handlers.clear()
    logging.shutdown()
