import abc
import logging
import pickle
import os
import platform
import pint
import json
import copy
import tkinter as tk
from tkinter.messagebox import showinfo
from tkinter import font
import pandas as pd
import numpy as np
import tikzplotlib as plt2tikz
import pyomo.environ as pyo

from cycler import cycler 
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
from matplotlib.lines import Line2D
from itertools import compress
from pathlib import Path
from clint.textui import progress

from pandastable import Table, TableModel, IndexHeader
import pandastable.config as pt_config
import pandastable.core as pt_core
import pandastable.util as pt_util

import secmod.helpers as helpers
import secmod.result_processing as result_processing
from secmod.classes import (
    Process, Product, config, units,get_target_unit_from_unit)

target_units = json.load(open("SecMOD/00-INPUT/00-RAW-INPUT/01-UNITS-TARGET-OPTIMIZATION.json", 'r'))
target_units_dictionary = {}
for target_unit in target_units:
    target_pint_unit = units(target_unit)
    target_units_dictionary[target_pint_unit.dimensionality] = target_unit

class AbstractFrame_evaluation(tk.Frame):
    """ This is the abstract GUI Frame class
    
    It is used to define the common parts of the MainFrame and the DetailedFrame."""
    def __init__(self,parent,optimization_results,working_directory,frac_height = 0.8, frac_width = 0.8):
        self.parent = parent
        self.optimization_results = optimization_results
        self.working_directory = working_directory
        self.parent.resizable(True,True)
        self.utilities={}
        # get width and height of screen
        self.utilities['screen_height'] = parent.winfo_screenheight()
        self.utilities['screen_width'] = parent.winfo_screenwidth()
        # define fraction of used screen
        self.utilities['fraction_height'] = frac_height
        self.utilities['fraction_width'] = frac_width
        # set width and height of window
        self.utilities['window_height'] = int(self.utilities['fraction_height']*self.utilities['screen_height'])
        self.utilities['window_width'] = int(self.utilities['fraction_width']*self.utilities['screen_width'])
        # define properties for plot items
        self.utilities['default_linewidth_areabar'] = 0
        self.utilities['default_linewidth_lineplot'] = 2
        self.utilities['changed_linewidth'] = 3.5
        self.utilities['changed_alpha'] = 1
        self.utilities['default_alpha'] = 0.8
        self.utilities['default_alpha_legend'] = 0.6
        self.utilities['min_shade']= 0.35
        self.utilities['colormap'] = helpers.get_rwth_colors()
        # set font
        if 'Arial' in font.families(): # if Arial available on operation system, select Arial (Corporate Design of the RWTH)
            self.utilities['standard_font'] = 'Arial'
            self.parent.option_add('*Font', self.utilities['standard_font'] + ' 9')
        # set geometry
        self.parent.geometry('{}x{}+{}+{}'.format(int(self.utilities['window_width']),int(self.utilities['window_height']),int((1-self.utilities['fraction_width'])/2*self.utilities['screen_width']),int((1-self.utilities['fraction_height'])/2*self.utilities['screen_height'])))
        # frames
        self.utilities['rows'] = [7,3] # height fraction of frames, important: only integer values
        self.utilities['columns'] = [6,4] # width fraction of frames, important: only integer values
        self.utilities['rowints'] = sum(self.utilities['rows'])
        self.utilities['columnints'] = sum(self.utilities['columns']) 
        # set sizes of Frames
        for [idxrow, row] in enumerate(self.utilities['rows']):
            self.parent.grid_rowconfigure(idxrow, weight=row, uniform="x")
        for [idxcolumn, column] in enumerate(self.utilities['columns']):
            self.parent.grid_columnconfigure(idxcolumn, weight=column, uniform="x")
        self.parent.grid_propagate(False)       
        # secmod icon
        if os.path.exists(str(self.working_directory)+'\\icon\\icon_secmod.ico'):
            self.parent.iconbitmap(r'icon\\icon_secmod.ico')
        ## setup Frame Layout
        # create frames
        self.frame_south = tk.Frame(self.parent)
        self.frame_north = tk.Frame(self.parent)
        self.frame_east = tk.Frame(self.parent)
        self.frame_southwest = tk.Frame(self.frame_south)
        self.frame_southeast = tk.Frame(self.frame_south)
        self.frame_south.grid(row = 1, column = 0,sticky = 'nsew')
        self.frame_north.grid(row = 0, column = 0,sticky = 'nsew')
        self.frame_east.grid(row = 0, column = 1,rowspan=2,sticky = 'nsew')
        self.frame_southwest.grid(row=0, column=0,sticky = 'nsew')
        self.frame_southeast.grid(row=0, column=1,sticky = 'nse')
        # adjust south south frame to south frame size
        self.frame_south.grid_columnconfigure(index = 1, weight = 1)
        self.frame_south.grid_rowconfigure(index = 1, weight = 1)
        # southeast frame
        self.button_save = tk.Button(self.frame_southeast,text = 'Save Plot/Data',command = self.savePlotData)
        self.button_save.grid(sticky='ne')
        # initialize attributes which are overridden by Child Class
        self.plot_type = None
        self.plot_types = None
        self.plotted_balance = None
        self.grouped_balance = None
        self.extended_balance = None
        self.unit_time_summation = None
        self.plotted_balance_name = None
        self.title_string = None
        self.selection_balance = None
        self.list_products = None
        self.selection_product = None
        self.codes_of_singleindex_balance = None

    def startPlot(self):
        """ start setup or plot of figure"""
        # if figure already created, change plot; otherwise first create figure 
        if not hasattr(self,'fig'):
            self.setupFigure()
        self.plotFigure()

    def setupFigure(self):
        """ set up figure in north frame"""
        # setup figure 
        matplotlib.style.use('fast')
        self.utilities['dpi']=100
        self.fig = Figure(figsize = (self.calculateFrameGeometry(0,0)['width']/self.utilities['dpi'],self.calculateFrameGeometry(0,0)['height']/self.utilities['dpi']))
        if 'standard_font' in self.utilities: # if standard_font manually set
            try: # if possible to set standard_font in matplotlib
                plt.rcParams.update({'font.family': self.utilities['standard_font']})
            except:
                logging.info('Unable to set Font in Plot. Used default Font instead.')
        # plot in north frame
        self.plot_canvas = FigureCanvasTkAgg(self.fig, master = self.frame_north)
        self.plot_canvas.get_tk_widget().grid(sticky = 'nsew')
    
    def calculateFrameGeometry(self,idxrow=0,idxcolumn=0):
        """ Calculate width and height of frame in GUI """
        geometry={'width': 0,'height': 0}
        geometry['width']= int(self.utilities['window_width']*self.utilities['columns'][idxcolumn]/self.utilities['columnints'])
        geometry['height']= int(self.utilities['window_height']*self.utilities['rows'][idxrow]/self.utilities['rowints'])
        return geometry
    
    def plotFigure(self):
        """ plot balance """
        if hasattr(self, 'annotation'): # remove annotation
            self.annotation.set_visible(False)
        self.fig.clf()
        self.axes = self.fig.add_axes([0.1,0.1,0.8,0.8])  
        # group balance by Checkbutton Selection (only in Detailed Frame)
        self.grouped_balance = self.groupBalanceByIndex(self.plotted_balance)
        # select plot type
        self.selectPlotType()
        # adjust axes
        self.axes.set_xlabel(self.grouped_balance.columns.name.replace('_',' ').title())
        unit_string = self.getbalanceUnits(self.grouped_balance,self.unit_time_summation)[0]
        self.axes.set_ylabel(self.selection_balance.get() + ' [{}]'.format(unit_string))
        # set legend
        self.createLegend()
        # title
        self.title_string = self.getTitleString()
        self.axes.set_title(self.title_string)
        # draw
        self.plot_canvas.draw()
        # show table
        self.showTable()  
        # highlight selection over which mouse hovers
        self.fig.canvas.mpl_connect('motion_notify_event', self.highlightMouseSelection)

    def getbalanceUnits(self,grouped_balance,unit_time_summation, b_extendedDataframe = False):
        """ Get units of balance in optimization"""
        unit_string = ''
        # get name of parameter
        if 'process' in grouped_balance.index.names:
            index_name_parameter = 'process'
        elif 'products' in grouped_balance.index.names:
            index_name_parameter = 'products'
        elif 'impact_category' in grouped_balance.index.names:
            index_name_parameter = 'impact_category'
        
        # create list of parameters in index level
        parameter_list = grouped_balance.index.get_level_values(index_name_parameter)
        unit_list = []
        # get unit and append to list
        for parameter in parameter_list:
            concat_unit_str = self.optimization_results['Utilities']['Unit_dict'][parameter]+unit_time_summation
            try: # if subunits in target_units_dictionary
                unit_list.append(get_target_unit_from_unit(concat_unit_str))
            except: # if not in dictionary
                unit_list.append(concat_unit_str)
                logging.warning('Cannot create unit string of {} from input. Used unit string instead'.format(concat_unit_str))
        if self.plot_type.get() == 'line': # if plot type == line plot, Unit is percent
            unit_string = '%'
        else:
            if not b_extendedDataframe:
                unit_set = list(np.unique(np.array(unit_list)[self.getNonzeroIndizes()])) # get unique list of units whose corresponding values are nonzero
            else:
                unit_set = list(np.unique(np.array(unit_list))) # get unique list of units
            # create unit_string
            for unit in unit_set:
                if unit == unit_set[0]:
                    unit_string+=unit
                else:
                    unit_string+='/'+unit
        return unit_string, unit_list

    def selectPlotType(self):
        """ plots the data according to specified plot type """
        colors = self.setColors()
        if self.plot_type.get() == self.plot_types[0][1]: # Area Plot
            try:
                self.plot_balance = self.grouped_balance[self.getNonzeroIndizes()].transpose().plot.area(ax=self.axes, linewidth=0,color = colors, alpha = self.utilities['default_alpha'], picker=True,stacked = True)
            except ValueError as valerr:
                self.plot_balance = self.grouped_balance[self.getNonzeroIndizes()].transpose().plot.area(ax=self.axes, linewidth=0,color = colors, alpha = self.utilities['default_alpha'], picker=True, stacked = False)
                logging.warning(str(valerr)+'\nUnstacked Area Plot used')
            except TypeError as typeerr:
                logging.warning(str(typeerr))
            self.plot_balance.set_xticks(self.grouped_balance.transpose().index.values)
            
        elif self.plot_type.get() == self.plot_types[1][1]: # Bar Plot
            try:
                self.plot_balance = self.grouped_balance[self.getNonzeroIndizes()].transpose().plot.bar(ax=self.axes, linewidth=0,color = colors,alpha = self.utilities['default_alpha'],stacked = True, picker=True)
                self.plot_balance.set_xticklabels(self.plot_balance.get_xticklabels(), rotation='horizontal')
            except TypeError as typeerr:
                logging.warning(str(typeerr))
        elif self.plot_type.get() == self.plot_types[2][1]: # Line Plot
            # divide by columns of first year to show development in percentage
            line_balance = self.grouped_balance.div(self.grouped_balance[self.grouped_balance.columns[0]],axis = 0).fillna(0)*100
            try:
                self.plot_balance = line_balance[self.getNonzeroIndizes()].transpose().plot(ax=self.axes,linewidth = self.utilities['default_linewidth_lineplot'],color = colors, alpha = self.utilities['default_alpha'], picker=5)
                self.plot_balance.set_xticks(self.grouped_balance.transpose().index.values)
            except TypeError as typeerr:
                logging.warning(str(typeerr))
        # set ylabel to scientific notation 10^n for n < -4 and n > 2 
        self.axes.ticklabel_format(axis='y',style='sci',scilimits=(-4,3))
    def createLegend(self):
        """ creates seperate Legends """          
        if isinstance(self, MainFrame_evaluation): # if in Main Frame
            if isinstance(self.grouped_balance.index, pd.MultiIndex): # if multiindex
                # first index
                level_index = 0
                custom_lines = [Line2D([0], [0], color=color, lw=4) for color in self.legend_maincolors] # create dummy lines
                custom_text = list(map(lambda x: str(x).title(),np.unique(self.grouped_balance.index.get_level_values(level_index)[self.getNonzeroIndizes()]))) # get list of level names that have non-zero values 
                legend_title = self.grouped_balance.index.names[level_index].replace('_',' ').title() # legend title
                self.axes.add_artist(self.axes.legend(custom_lines,custom_text,loc='lower left',title = legend_title,fontsize = 'small',framealpha = self.utilities['default_alpha_legend'])) # add legend
                # second index
                level_index = 1
                custom_lines = [Line2D([0], [0], color=color, lw=4) for color in self.legend_shades] # create dummy lines
                custom_text = list(map(lambda x: str(x).title(),np.unique(self.grouped_balance.index.get_level_values(level_index)[self.getNonzeroIndizes()]))) # get list of level names that have non-zero values 
                legend_title = self.grouped_balance.index.names[1].replace('_',' ').title() # legend title
                self.axes.add_artist(self.axes.legend(custom_lines,custom_text,loc='lower right',title = legend_title,fontsize = 'small',framealpha = self.utilities['default_alpha_legend'])) # add legend
            else: # no multiindex
                custom_lines = [Line2D([0], [0], color=color, lw=4) for color in self.legend_maincolors] # create dummy lines   
                custom_text = list(map(lambda x: str(x).title(),np.unique(self.grouped_balance.index.values[self.getNonzeroIndizes()]))) # get list of level names that have non-zero values 
                legend_title = self.grouped_balance.index.names[0].replace('_',' ').title() # legend title
                self.axes.add_artist(self.axes.legend(custom_lines,custom_text,loc='lower left',title = legend_title,fontsize = 'small',framealpha = self.utilities['default_alpha_legend'])) # add legend
        elif isinstance(self, DetailedFrame_evaluation): # if in Detailed Frame
            self.axes.get_legend().remove() # if none of the indizes below --> remove legend
            if 'nodes' in self.grouped_balance.index.names: # select level of nodes
                level_index = self.grouped_balance.index.names.index('nodes')
                custom_lines = [Line2D([0], [0], color=color, lw=4) for color in self.legend_maincolors] # create dummy lines  
                custom_text = np.unique(self.grouped_balance.index.get_level_values(level_index)[self.getNonzeroIndizes()]) # get list of level names that have non-zero values 
                legend_title = self.grouped_balance.index.names[level_index].replace('_',' ').title() # legend title
                self.axes.add_artist(self.axes.legend(custom_lines,custom_text,loc='lower left',title = legend_title,fontsize = 'small',framealpha = self.utilities['default_alpha_legend'])) # add legend
            elif 'connections' in self.grouped_balance.index.names: # select level of connections
                level_index = self.grouped_balance.index.names.index('connections')
                custom_lines = [Line2D([0], [0], color=color, lw=4) for color in self.legend_maincolors] # create dummy lines   
                custom_text = np.unique(self.grouped_balance.index.get_level_values(level_index)[self.getNonzeroIndizes()]) # get list of level names that have non-zero values
                legend_title = self.grouped_balance.index.names[level_index].replace('_',' ').title() # legend title
                self.axes.add_artist(self.axes.legend(custom_lines,custom_text,loc='lower left',title = legend_title,fontsize = 'small',framealpha = self.utilities['default_alpha_legend'])) # add legend
            if 'construction_years' in self.grouped_balance.index.names: # select level of construction years
                level_index = self.grouped_balance.index.names.index('construction_years')
                custom_lines = [Line2D([0], [0], color=color, lw=4) for color in self.legend_shades] # create dummy lines  
                custom_text = np.unique(self.grouped_balance.index.get_level_values(level_index)[self.getNonzeroIndizes()]) # get list of level names that have non-zero values 
                legend_title = self.grouped_balance.index.names[level_index].replace('_',' ').title() # legend title
                self.axes.add_artist(self.axes.legend(custom_lines,custom_text,loc='lower right',title = legend_title,fontsize = 'small',framealpha = self.utilities['default_alpha_legend'])) # add legend

    def setColors(self):
        """ return list of colors for plot """    
        if isinstance(self.grouped_balance.index, pd.MultiIndex): # if multiindex
            if isinstance(self, MainFrame_evaluation): # if in Main Frame
                # main colors
                level_index = 0
                nonzero_main = self.grouped_balance.index.codes[level_index][self.getNonzeroIndizes()] # get main indizes which are nonzero
                nonzero_main_unique = np.unique(nonzero_main) # get unique values of nonzero main indizes
                self.legend_maincolors = self.utilities['colormap'][nonzero_main_unique%len(self.utilities['colormap'])] # get main colors correlating with main indices
                maincolors = np.array([self.legend_maincolors[nonzero_main_unique == main_index] for main_index in nonzero_main]).reshape(len(nonzero_main),3) # create color array correlating with main index      
                # shades
                level_index = 1
                nonzero_shades = self.grouped_balance.index.codes[level_index][self.getNonzeroIndizes()] # get lesser indizes which are nonzero
                nonzero_shades_unique = np.unique(nonzero_shades) # get unique values of nonzero lesser indizes
                self.legend_shades = np.linspace(1.0,self.utilities['min_shade'],len(nonzero_shades_unique)).reshape(len(nonzero_shades_unique),1) # create shades by percentage
                shades = np.array([self.legend_shades[nonzero_shades_unique == shade] for shade in nonzero_shades]).reshape(len(nonzero_shades),1) # create color array correlating with lesser index
            elif isinstance(self, DetailedFrame_evaluation): # if in Detailed Frame
                maincolors = np.tile(self.utilities['colormap'][0],(len(self.grouped_balance[self.getNonzeroIndizes()]),1)) # default main color. Just one main color, so repeat for length of index
                shades = np.array(1) 
                self.legend_shades = shades
                if 'nodes' in self.grouped_balance.index.names: # select level of nodes
                    level_index = self.grouped_balance.index.names.index('nodes')
                    nonzero_main = self.grouped_balance.index.codes[level_index][self.getNonzeroIndizes()] # get main indizes which are nonzero
                    nonzero_main_unique = np.unique(nonzero_main) # get unique values of nonzero main indizes
                    self.legend_maincolors = self.utilities['colormap'][nonzero_main_unique%len(self.utilities['colormap'])] # get main colors correlating with index of nodes
                    maincolors = np.array([self.legend_maincolors[nonzero_main_unique == main_index] for main_index in nonzero_main]).reshape(len(nonzero_main),3) # create color array correlating with index of nodes  
                elif 'connections' in self.grouped_balance.index.names: # select level of connections
                    level_index = self.grouped_balance.index.names.index('connections')
                    nonzero_main = self.grouped_balance.index.codes[level_index][self.getNonzeroIndizes()] # get main indizes which are nonzero
                    nonzero_main_unique = np.unique(nonzero_main) # get unique values of nonzero main indizes
                    self.legend_maincolors = self.utilities['colormap'][nonzero_main_unique%len(self.utilities['colormap'])] # get main colors correlating with main indices
                    maincolors = np.array([self.legend_maincolors[nonzero_main_unique == main_index] for main_index in nonzero_main]).reshape(len(nonzero_main),3) # create color array correlating with main index
                if 'construction_years' in self.grouped_balance.index.names: # select level of construction years
                    level_index = self.grouped_balance.index.names.index('construction_years')
                    nonzero_shades = self.grouped_balance.index.codes[level_index][self.getNonzeroIndizes()] # get lesser indizes which are nonzero
                    nonzero_shades_unique = np.unique(nonzero_shades) # get unique values of nonzero lesser indizes
                    self.legend_shades = np.linspace(1.0,self.utilities['min_shade'],len(nonzero_shades_unique)).reshape(len(nonzero_shades_unique),1) # create shades by percentage
                    shades = np.array([self.legend_shades[nonzero_shades_unique == shade] for shade in nonzero_shades]).reshape(len(nonzero_shades),1) # create color array correlating with index of construction years
            endcolors = np.multiply(maincolors, shades) # multiply maincolors with shades
            main_black = np.tile(0.6,3) # grayscale
            self.legend_shades = np.multiply(main_black,self.legend_shades.reshape(self.legend_shades.size,1)) # create color of first maincolor with used shades
        else: # no multiindex
            # endcolors = np.array([self.utilities['colormap'][code%len(self.utilities['colormap'])] for code in range(self.grouped_balance.index.size)])
            if self.selection_product.get() != self.list_products[0] and isinstance(self, MainFrame_evaluation): # single product and Mainframe
                maincolors = self.utilities['colormap'][self.codes_of_singleindex_balance[self.getNonzeroIndizes()]%len(self.utilities['colormap'])]
            else: 
                maincolors = self.utilities['colormap'][np.arange(self.grouped_balance.index.size)[self.getNonzeroIndizes()]%len(self.utilities['colormap'])]
            self.legend_maincolors = maincolors
            # endcolors = np.ones((self.grouped_balance.index.size,3)) # create array of ones
            # endcolors[self.getNonzeroIndizes()] = maincolors # create array correlating with main index, if values zero -> set to 1   
            endcolors = maincolors
        return endcolors

    def highlightMouseSelection(self,event):
        """ highlight selection over which mouse currently hovers """
        def getAnnotation(selection_label,event):
            """ annonate cursor with label of current selection """ 
            if not hasattr(self, 'annotation'): # if no already existing annotation 
                self.annotation = self.axes.text(event.xdata, event.ydata, selection_label.title(),zorder = 6,bbox = dict(boxstyle='round',fc='0.5',alpha=self.utilities['default_alpha_legend'])) # (zorder puts annotation on top of legend)
            elif not self.annotation.get_visible(): # if already existing but invisible
                self.annotation = self.axes.text(event.xdata, event.ydata, selection_label.title(),zorder = 6,bbox = dict(boxstyle='round',fc='0.5',alpha=self.utilities['default_alpha_legend']))
            else:
                self.annotation.set_position((event.xdata,event.ydata))
                self.annotation.set_text(selection_label.title())
            event_in_any_plot_member = True # set bool if mouse over any plot member to False
            return event_in_any_plot_member

        if event.inaxes == self.axes:
            event_in_any_plot_member = False # set bool if mouse over any plot member to False
            if self.plot_type.get() == self.plot_types[0][1]: # area plot
                for collection in self.axes.collections:
                    # Searching which data member corresponds to current mouse position
                    if collection.contains(event)[0]:
                        collection.set_linewidth(self.utilities['changed_linewidth'])
                        collection.set_alpha(self.utilities['changed_alpha'])
                        event_in_any_plot_member = getAnnotation(collection.get_label(),event) # get Annotation
                    else:
                        collection.set_linewidth(self.utilities['default_linewidth_areabar'])
                        collection.set_alpha(self.utilities['default_alpha'])
            elif self.plot_type.get() == self.plot_types[1][1]: # bar plot
                for container in self.axes.containers:
                    for bar in container:
                        if bar.contains(event)[0]:
                            bar.set_linewidth(self.utilities['changed_linewidth'])
                            bar.set_alpha(self.utilities['changed_alpha'])
                            event_in_any_plot_member = getAnnotation(container.get_label(),event) # get Annotation
                        else:
                            bar.set_linewidth(self.utilities['default_linewidth_areabar'])
                            bar.set_alpha(self.utilities['default_alpha'])
            else: # line plot
                for line in self.axes.get_lines():
                    # Searching which data member corresponds to current mouse position
                    if line.contains(event)[0]:
                        line.set_linewidth(self.utilities['changed_linewidth'])
                        line.set_alpha(self.utilities['changed_alpha'])
                        event_in_any_plot_member = getAnnotation(line.get_label(),event) # get Annotation
                    else:
                        line.set_linewidth(self.utilities['default_linewidth_lineplot'])
                        line.set_alpha(self.utilities['default_alpha'])
            if hasattr(self, 'annotation') and not event_in_any_plot_member: # if  already existing annotation and mouse not over plot member
                self.annotation.set_visible(False) # set annotation to invisible
            # draw
            self.plot_canvas.draw_idle()

    def showTable(self,extendedDataframe = False):
        """ Show table with values of dataframe """
        if extendedDataframe: # if extended Dataframe selected in Detailed Frame --> create Table for extended balance
            grouped_extended_balance = self.groupBalanceByIndex(self.extended_balance)
            self.balance_table = copy.deepcopy(grouped_extended_balance)
            unit_list = self.getbalanceUnits(grouped_extended_balance,self.unit_time_summation, b_extendedDataframe = True)[1]
        else: # otherwise for plotted balance   
            self.balance_table = copy.deepcopy(self.grouped_balance)
            unit_list = self.getbalanceUnits(self.grouped_balance,self.unit_time_summation)[1]
        # insert unit column
        self.balance_table.insert(0,'unit',unit_list)
        if not hasattr(self,'table'): # if table not yet created
            self.table = Table(self.frame_east, dataframe=self.balance_table)
            self.table.grid()
            pt_config.apply_options({'fontsize':8,'cellwidth':40},self.table)
            if 'standard_font' in self.utilities: # if standard_font manually set
                if self.utilities['standard_font'] in pt_util.getFonts(): # if possible to set standard_font in table
                    pt_config.apply_options({'font': self.utilities['standard_font']},self.table)       
                else:
                    logging.info('Unable to set Font in Table. Used default Font instead.')
            self.table.show()
            self.table.showIndex()
        else:
            self.table.updateModel(TableModel(self.balance_table))
            self.table.rowindexheader = IndexHeader(self.table.parentframe, self.table)
            self.table.rowindexheader.grid(row=0,column=0,rowspan=1,sticky='news')
            self.adjustManualColumnWidths()
            self.table.redraw()

    def adjustManualColumnWidths(self):
        """ Adjust column width of table.
        Copied from pandastable documentation and cleared line "if w > 200 """
        self.table.cols = self.table.model.getColumnCount()
        for col in range(self.table.cols):
            colname = self.table.model.getColumnName(col)
            l = self.table.model.getlongestEntry(col)
            txt = ''.join(['X' for i in range(l+1)])
            tw = pt_util.getTextLength(txt, self.table.maxcellwidth,
                                       font=self.table.thefont)[0]
            if tw >= self.table.maxcellwidth:
                tw = self.table.maxcellwidth
            elif tw < self.table.cellwidth:
                tw = self.table.cellwidth
            self.table.columnwidths[colname] = tw

    def savePlotData(self):
        """ opens new Save Frame """
        self.newWindow = tk.Toplevel(self.parent)
        SaveFrame(self.newWindow, self)

    def groupBalanceByIndex(self,input_balance):
        """ in mainframe no grouping necessary. Method overwritten in Detailed Frame """
        return input_balance

    def getNonzeroIndizes(self,summed_level = None):
        """ get indizes of balance whose values are nonzero """
        if summed_level is not None: # level specified
            balance_by_level = self.grouped_balance.sum(level = summed_level)
        else: # no level specified
            balance_by_level = self.grouped_balance
        # drop rows that are zero
        b_nonzero = list((balance_by_level != 0).any(axis=1)) # get entries that are nonzero
        # if b_nonzero empty, select at least first row
        if True not in b_nonzero and b_nonzero:
            b_nonzero[0] = True 
        return b_nonzero   

    @abc.abstractmethod
    def getTitleString(self):
        """ abstract method. Needs to be implemented in Child Class"""

class MainFrame_evaluation(AbstractFrame_evaluation):
    """ This is the Main Frame of the evaluation GUI
    
    It is a child class of AbstractFrame_evaluation"""
    def __init__(self,parent,optimization_results,working_directory):
        """ sets up Main Frame of Evaluation GUI """
        # setup Abstract Frame
        super().__init__(parent,optimization_results,working_directory,frac_height=0.8, frac_width=0.9)
        self.parent.title('SecMOD Data Viewer')
        ## Southwest Frame:
        # Level 0: Select Product
        tk.Label(self.frame_southwest,text = 'Select Product',bg = 'grey').grid(row = 0,column=0,sticky='ew')
        self.list_products = [product.title() for product in self.optimization_results['Utilities']['Parameters']['products']]
        self.list_products.insert(0,'All Products')
        self.selection_product = tk.StringVar()
        self.selection_product.set(self.list_products[0])
        self.option_menu_product = tk.OptionMenu(self.frame_southwest, self.selection_product, *self.list_products,command=self.getDataframeFromSelection)
        self.option_menu_product.grid(row=1,column=0, sticky = 'w')
        
        # Level 1: Select Balance
        tk.Label(self.frame_southwest,text = 'Select Balance',bg = 'grey').grid(row = 0,column=1,sticky='ew')
        self.list_balances = ['Product Flow','Demand','Capacities', 'Impacts']
        self.selection_balance = tk.StringVar()
        self.selection_balance.set(self.list_balances[0])
        self.option_menu_balance = tk.OptionMenu(self.frame_southwest, self.selection_balance, *self.list_balances,command=self.getDataframeFromSelection)
        self.option_menu_balance.grid(row=1,column=1, sticky = 'w')
        
        # Level 2 + 3: Select Impact/Process specification
        self.setUpRadiobuttons()
        self.selection_balance.trace('w',self.setUpRadiobuttons) # if selection_balance changes, set up radiobuttons
        self.selection_product.trace('w',self.setUpRadiobuttons) # if selection_product changes, set up radiobuttons

        # Level 4: Select Plot Type
        tk.Label(self.frame_southwest,text = 'Select Plot Type',bg = 'grey').grid(row = 0,column = 4,sticky='ew')
        self.plot_types = [
            ('Area Plot','area',1),
            ('Bar Plot','bar',2),
            ('Line Plot','line',3)
        ]
        self.plot_type = tk.StringVar()
        self.plot_type.set(self.plot_types[0][1])
        for txt_plot_type,value_plot_type, row_plot_type in self.plot_types:
            tk.Radiobutton(self.frame_southwest,
                            text = txt_plot_type,
                            variable = self.plot_type,
                            command = self.plotFigure,
                            value = value_plot_type).grid(row = row_plot_type,column = 4,sticky ='w')
        
        ## get Dataframe and start Plot
        self.getDataframeFromSelection()

    def setUpRadiobuttons(self,*args):
        """ Set up and change radiobuttons for product flows/impacts/processes """
        self.dict_radiobuttons = {}
        # delete current widgets
        list_widgets = []
        for widget in self.frame_southwest.children:
            if self.frame_southwest.children[widget].grid_info()['column'] in [2,3]:
                list_widgets.append(widget)
        # delete every widget with column = 2 or 3
        for widget in list_widgets:
            self.frame_southwest.children[widget].destroy()
        # Product flows
        self.list_product_flows = [
            ('Total Product Flows','total',1),
            ('Production Processes','production',2),
            ('Storage Processes','storage',3),
            ('Transshipment Processes','transshipment',4),
            ('Transmission Processes','transmission',5)
        ]
        self.selection_product_flows = tk.StringVar()
        self.selection_product_flows.set(self.list_product_flows[0][1])
        # Demand
        self.list_demand = [
            ('Demand','demand',1),
            ('Non Served Demand','non_served_demand',2),
        ]
        self.selection_demand = tk.StringVar()
        self.selection_demand.set(self.list_demand[0][1])
        # Processes
        self.list_processes = [
            ('Production Processes','production',1),
            ('Storage Processes','storage',2),
            ('Transshipment Processes','transshipment',3),
            ('Transmission Processes','transmission',4)
        ]
        self.selection_process = tk.StringVar()
        self.selection_process.set(self.list_processes[0][1])
        # Impacts
        # impact type
        self.list_impacts = [
            ('Total Impact','total',1),
            ('Invest Impact','invest',2),
            ('Operational Impact','operational',3)
        ]
        self.selection_impact = tk.StringVar()
        self.selection_impact.set(self.list_impacts[0][1])
        # impact of processes/non served demand
        self.list_impact_process = [
                ('All Categories','total',1),
                ('Production Processes','production',2),
                ('Storage Processes','storage',3),
                ('Transshipment Processes','transshipment',4),
                ('Transmission Processes','transmission',5),
                ('Non Served Demand','non_served_demand',6)
            ]
        self.selection_impact_process = tk.StringVar()
        if self.selection_product.get() == self.list_products[0]: # if all products selected
            self.selection_impact_process.set(self.list_impact_process[0][1])
        else: # if single product selected, not impacts of all categories selectable
            self.selection_impact_process.set(self.list_impact_process[1][1])

        if self.selection_balance.get() == self.list_balances[0]: # Product Flows
            tk.Label(self.frame_southwest,text = 'Select Product Flow by Process Category',bg = 'grey').grid(row = 0,column=2,sticky='ew')
            # radiobuttons of PROCESS Category
            for txt_selection_product_flows,value_selection_product_flows, row_selection_product_flows in self.list_product_flows:
                self.dict_radiobuttons[value_selection_product_flows] = tk.Radiobutton(
                    self.frame_southwest,
                    text = txt_selection_product_flows,
                    variable = self.selection_product_flows,
                    command = self.getDataframeFromSelection,
                    value = value_selection_product_flows)
                self.dict_radiobuttons[value_selection_product_flows].grid(row = row_selection_product_flows,column = 2,sticky ='w')
        elif self.selection_balance.get() == self.list_balances[1]: # Demand         
            tk.Label(self.frame_southwest,text = 'Select Demand',bg = 'grey').grid(row = 0,column=2,sticky='ew')
            # radiobuttons of DEMAND Type
            for txt_selection_demand,value_selection_demand, row_selection_demand in self.list_demand:
                tk.Radiobutton(
                    self.frame_southwest,
                    text = txt_selection_demand,
                    variable = self.selection_demand,
                    command = self.getDataframeFromSelection,
                    value = value_selection_demand).grid(row = row_selection_demand,column = 2,sticky ='w')     
        elif self.selection_balance.get() == self.list_balances[2]: # Capacities
            tk.Label(self.frame_southwest,text = 'Select Processes',bg = 'grey').grid(row = 0,column=2,sticky='ew')
            # radiobuttons of PROCESS Category
            for txt_selection_process,value_selection_process, row_selection_process in self.list_processes:
                self.dict_radiobuttons[value_selection_process] = tk.Radiobutton(
                    self.frame_southwest,
                    text = txt_selection_process,
                    variable = self.selection_process,
                    command = self.getDataframeFromSelection,
                    value = value_selection_process)
                self.dict_radiobuttons[value_selection_process].grid(row = row_selection_process,column = 2,sticky ='w')
        elif self.selection_balance.get() == self.list_balances[3]: # Impacts
            tk.Label(self.frame_southwest,text = 'Select Impact Category',bg = 'grey').grid(row = 0,column=2,sticky='ew')
            # radiobuttons of IMPACT Type
            for txt_selection_impact,value_selection_impact, row_selection_impact in self.list_impacts:
                tk.Radiobutton(self.frame_southwest,
                            text = txt_selection_impact,
                            variable = self.selection_impact,
                            command =self.getDataframeFromSelection,
                            value = value_selection_impact).grid(row = row_selection_impact,column = 2,sticky ='w')
            tk.Label(self.frame_southwest,text = 'Select Impacts by Producer',bg = 'grey').grid(row = 0,column=3,sticky='ew')
            # radiobuttons of IMPACT Producer
            for txt_selection_impact_process,value_selection_impact_process, row_selection_impact_process in self.list_impact_process:
                self.dict_radiobuttons[value_selection_impact_process] = tk.Radiobutton(self.frame_southwest,
                    text = txt_selection_impact_process,
                    variable = self.selection_impact_process,
                    command =self.getDataframeFromSelection,
                    value = value_selection_impact_process)
                self.dict_radiobuttons[value_selection_impact_process].grid(row = row_selection_impact_process,column = 3,sticky ='w')
                if self.selection_product.get() != self.list_products[0] and row_selection_impact_process == 1: # if single product and first radiobutton, disable radiobutton
                    self.dict_radiobuttons[value_selection_impact_process].config(state=tk.DISABLED) 

    def getDataframeFromSelection(self,*args):
        """ Create Dataframe which contains the information representing the selection,
        i.e. selection of Product, Balance and Impact/Capacities      
        """
        # get Balance by selection balances
        if self.selection_balance.get() == self.list_balances[0]: # Product Flows
            if self.selection_product_flows.get() == self.list_product_flows[0][1]: # Total Product Flows
                self.plotted_balance_name = 'product_flow'
            elif self.selection_product_flows.get() == self.list_product_flows[1][1]: # Production processes
                self.plotted_balance_name = 'product_flow_production'
            elif self.selection_product_flows.get() == self.list_product_flows[2][1]: # Storage processes
                self.plotted_balance_name = 'product_flow_storage'
            elif self.selection_product_flows.get() == self.list_product_flows[3][1]: # Transshipment processes
                self.plotted_balance_name = 'product_flow_transshipment'
            elif self.selection_product_flows.get() == self.list_product_flows[4][1]: # Transmission processes
                self.plotted_balance_name = 'product_flow_transmission'
        elif self.selection_balance.get() == self.list_balances[1]: # Demand
            if self.selection_demand.get() == self.list_demand[0][1]: # Demand
                self.plotted_balance_name = 'demand'
            elif self.selection_demand.get() == self.list_demand[1][1]: # Non Served Demand
                self.plotted_balance_name = 'non_served_demand'
        elif self.selection_balance.get() == self.list_balances[2]: # Capacities
            if self.selection_process.get() == self.list_processes[0][1]: # Production Processes
                self.plotted_balance_name = 'existing_capacity_production'
            elif self.selection_process.get() == self.list_processes[1][1]: # Storage Processes
                self.plotted_balance_name = 'existing_capacity_storage'
            elif self.selection_process.get() == self.list_processes[2][1]: # Transshipment Processes
                self.plotted_balance_name = 'existing_capacity_transshipment'
            elif self.selection_process.get() == self.list_processes[3][1]: # Transmission Processes
                self.plotted_balance_name = 'existing_capacity_transmission'
        elif self.selection_balance.get() == self.list_balances[3]: # Impacts
            if self.selection_impact.get() == self.list_impacts[0][1]: # Total Impact
                self.plotted_balance_name = 'total_impact'
            elif self.selection_impact.get() == self.list_impacts[1][1]: # Invest Impact
                self.plotted_balance_name = 'invest_impact'
            elif self.selection_impact.get() == self.list_impacts[2][1]: # Operational Impact
                self.plotted_balance_name = 'operational_impact'
            # select by Category
            if self.selection_impact_process.get() == self.list_impact_process[1][1]: # Production Processes
                self.plotted_balance_name += '_production'
            elif self.selection_impact_process.get() == self.list_impact_process[2][1]: # Storage Processes
                self.plotted_balance_name += '_storage'
            elif self.selection_impact_process.get() == self.list_impact_process[3][1]: # Transshipment Processes
                self.plotted_balance_name += '_transshipment'
            elif self.selection_impact_process.get() == self.list_impact_process[4][1]: # Transmission Processes
                self.plotted_balance_name += '_transmission'
            elif self.selection_impact_process.get() == self.list_impact_process[5][1]: # Non Served Demand
                self.plotted_balance_name += '_non_served_demand'
        # get balance from optimization results
            # plotted balance is already grouped --> grouping in mainframe not necessary
            # for detailed frame plotted extended balance
        self.plotted_balance = self.optimization_results['Results'][self.plotted_balance_name+'_grouped']
        self.plotted_extended_balance = self.optimization_results['Results'][self.plotted_balance_name]
        self.unit_time_summation = self.optimization_results['Utilities']['Unit_time_summation'][self.plotted_balance_name]

        # SINGLE PRODUCT
        if self.selection_product.get() != self.list_products[0]: 
            if self.selection_balance.get() == self.list_balances[0]: # Product Flows
                # get list of products in balance
                product_list_balance = self.plotted_balance.index.get_level_values('products')
                # get indices of corresponding products
                idx_list = [product_index for product_index, product_name in enumerate(product_list_balance) if product_name == self.selection_product.get().lower()]
            elif self.selection_balance.get() == self.list_balances[1]: # Demand
                # get list of products in balance
                product_list_balance = self.plotted_balance.index.get_level_values('products')
                # get indices of corresponding products
                idx_list = [product_index for product_index, product_name in enumerate(product_list_balance) if product_name == self.selection_product.get().lower()]
            elif self.selection_balance.get() == self.list_balances[2]: # Capacities
                # get list of processes in balance
                process_list_balance = self.plotted_balance.index.get_level_values('process')
                # get indices of processes of corresponding products
                idx_list = [process_index for process_index, process_name in enumerate(process_list_balance) if process_name in self.optimization_results['Utilities']['ProductsOfProcess'][self.selection_process.get()+'_products'][self.selection_product.get().lower()]]
            elif self.selection_balance.get() == self.list_balances[3]: # Impacts
                if self.selection_impact_process.get() == self.list_impact_process[0][1]: # All
                    idx_list = range(self.plotted_balance.shape[0])
                elif self.selection_impact_process.get() == self.list_impact_process[5][1]: # Non Served Demand
                    # get list of products in balance
                    product_list_balance = self.plotted_balance.index.get_level_values('impact_products')
                    # get indices of corresponding products
                    idx_list = [product_index for product_index, product_name in enumerate(product_list_balance) if product_name == self.selection_product.get().lower()]
                else: # Processes
                    # get list of processes in balance
                    process_list_balance = self.plotted_balance.index.get_level_values('impact_process')
                    # get indices of processes of corresponding products
                    idx_list = [process_index for process_index, process_name in enumerate(process_list_balance) if process_name in self.optimization_results['Utilities']['ProductsOfProcess'][self.selection_impact_process.get()+'_products'][self.selection_product.get().lower()]]
            self.plotted_balance = self.plotted_balance.iloc[idx_list]
            if not isinstance(self.plotted_balance.index, pd.MultiIndex): # if not multiindex
                self.codes_of_singleindex_balance = np.array(idx_list)

        if self.plotted_balance.empty: # empty Dataframe selected
            logging.warning('Empty Dataframe selected, since no {} matching {}. Set Balance to Power Flow'.format(self.selection_balance.get(),self.selection_product.get()))
            self.selection_balance.set(self.list_balances[0]) # set balance to power flow
            self.getDataframeFromSelection() # get new Dataframe
        # enable/disable radiobuttons and checkbuttons
        self.disableEnableRadiobuttons()
        # plot
        self.startPlot()

    def disableEnableRadiobuttons(self):
        """ Disable radiobuttons of balance specification, which is not available for product 

        Only necessary for single product"""
        # SINGLE PRODUCT
        if self.selection_product.get() != self.list_products[0]: 
            # disable radiobutton of process category which no process corresponding to selected product
            for process_category in self.list_processes:
                # process category has no process corresponding to selected product
                if not self.optimization_results['Utilities']['ProductsOfProcess'][process_category[1]+'_products'][self.selection_product.get().lower()]:
                    if self.dict_radiobuttons:
                        self.dict_radiobuttons[process_category[1]].config(state=tk.DISABLED)          

    def plotFigure(self):
        """ plot balance 
        
        inherited from AbstractFrame_evaluation but click functionality added"""
        super().plotFigure()
        # click on plot to get detailed view
        self.fig.canvas.mpl_connect('pick_event', self.openDetailedWindow)  

    def openDetailedWindow(self, event):
        """ open Detailed Frame to further inspect line/bar/area clicked on"""
        # get set of indices of selected line/bar/area
        if hasattr(event, 'artist'): # mouse on plot
            if self.plot_type.get() != self.plot_types[1][1]: # if not bar plot
                self.selected_label = event.artist.get_label().strip('()')
                label_set = set(self.selected_label.split(', '))
            else: # bar plot
                label_list = [container.get_label() for container in event.artist.axes.containers if event.artist in container.get_children()]
                self.selected_label = label_list[0].strip('()')
                label_set = set(self.selected_label.split(', '))
            # get dataframe of selected line/bar/area
            index_list = [index for index in self.plotted_extended_balance.index if label_set.issubset(set(index))]
            reduced_balance = self.plotted_extended_balance.loc[index_list]
            # create new DetailedFrame
            self.newWindow = tk.Toplevel(self.parent)
            DetailedFrame_evaluation(self.newWindow, self.optimization_results, self.working_directory,reduced_balance,self)

    def getTitleString(self):
        """ Get Title String depended on selection of balance """
        if self.selection_balance.get() == self.list_balances[0]: # Product Flows
            title_string = 'Product Flow' 
            if self.selection_product_flows.get() != self.list_product_flows[0][1]:
                title_string += ' of ' + self.selection_product_flows.get().title() + ' Processes'
        elif self.selection_balance.get() == self.list_balances[1]: # Demand
            title_string = self.selection_demand.get().replace('_',' ').title()
        elif self.selection_balance.get() == self.list_balances[2]: # Capacities
            title_string = 'Existing Capacity of ' + self.selection_process.get().title() + ' Processes'
        elif self.selection_balance.get() == self.list_balances[3]: # Impacts
            title_string = self.selection_impact.get().title() + ' Impact'
            if self.selection_impact_process.get() != self.list_impact_process[0][1]: # not total
                if self.selection_impact_process.get() != self.list_impact_process[5][1]: # process category
                    title_string += ' of ' + self.selection_impact_process.get().title() + ' Processes'
                else: # not non served demand
                    title_string += ' of ' + self.selection_impact_process.get().replace('_',' ').title()
        if self.selection_product.get() != self.list_products[0]: # single product
            title_string += ' for ' + self.selection_product.get()
        return title_string

class DetailedFrame_evaluation(AbstractFrame_evaluation):
    """ This is the Detailed Frame of the evaluation GUI
     
    It is a child class of AbstractFrame_evaluation"""
    def __init__(self,parent,optimization_results,working_directory,reduced_balance,mainframe):
        """ sets up Detailed Frame of Evaluation GUI """
        super().__init__(parent,optimization_results,working_directory,frac_height=0.8, frac_width=0.9)
        self.parent.title('Detailed SecMOD Data Viewer')
        self.parent.grab_set()
        # copy values from mainframe
        self.mainframe = mainframe
        self.plotted_balance = reduced_balance
        self.plotted_balance_name = self.mainframe.plotted_balance_name
        self.extended_balance = self.mainframe.plotted_extended_balance
        self.list_balances = self.mainframe.list_balances
        self.list_products = self.mainframe.list_products
        self.list_impact_process = self.mainframe.list_impact_process
        self.selection_balance = self.mainframe.selection_balance
        self.selection_product_flows = self.mainframe.selection_product_flows
        self.selection_demand = self.mainframe.selection_demand
        self.selection_process = self.mainframe.selection_process
        self.selection_impact = self.mainframe.selection_impact
        self.selection_impact_process = self.mainframe.selection_impact_process
        self.selection_product = self.mainframe.selection_product
        self.unit_time_summation = self.mainframe.unit_time_summation 
        self.codes_of_singleindex_balance = self.mainframe.codes_of_singleindex_balance
        # setup Frame Layout
        # Level 0: Select Level of Aggregation
        tk.Label(self.frame_southwest,text = 'Sum Results over',bg = 'grey').grid(row = 0,column=0,sticky='ew')
        self.b_nodes = tk.IntVar()
        self.b_nodes.set(1)
        self.b_time_slices = tk.IntVar()
        self.b_time_slices.set(1)
        self.b_construction_years = tk.IntVar()
        self.b_construction_years.set(1)
        self.checkbutton_nodes = tk.Checkbutton(self.frame_southwest,text='nodes/connections',variable = self.b_nodes, command=self.startPlot)
        self.checkbutton_construction_years = tk.Checkbutton(self.frame_southwest,text='construction years',variable = self.b_construction_years, command=self.startPlot)
        self.checkbutton_nodes.grid(row = 1,column=0, sticky = 'w')
        self.checkbutton_construction_years.grid(row = 2,column=0, sticky = 'w')
        # comment toggled since of no suitable use yet
        # self.checkbutton_time_slices = tk.Checkbutton(self.frame_southwest,text='time slices',variable = self.b_time_slices, command=lambda:self.plotFigure()) 
        # self.checkbutton_time_slices.grid(row = 3,column=0, sticky = 'w')
        
        # Level 1: Select Plot Type
        tk.Label(self.frame_southwest,text = 'Select Plot Type',bg = 'grey').grid(row = 0,column = 1,sticky='ew')
        self.plot_types = [
            ('Area Plot','area',1),
            ('Bar Plot','bar',2),
            ('Line Plot','line',3)
        ]
        self.plot_type = tk.StringVar()
        self.plot_type.set(self.mainframe.plot_type.get())
        for txt_plot_type,value_plot_type, row_plot_type in self.plot_types:
            tk.Radiobutton(self.frame_southwest,
                            text = txt_plot_type,
                            variable = self.plot_type,
                            command = self.plotFigure,
                            value = value_plot_type).grid(row = row_plot_type,column = 1,sticky ='w')

        # Level 2: Select DataFrame of Table
        tk.Label(self.frame_southwest,text = 'Show Table for',bg = 'grey').grid(row = 0,column = 2,sticky='ew')
        self.table_options = [
            ('Detailed Dataset','detailed',1),
            ('Entire Dataset','total',2)
        ]
        self.table_option = tk.StringVar()
        self.table_option.set(self.table_options[0][1])
        for txt_table_option,value_table_option, row_table_option in self.table_options:
            tk.Radiobutton(self.frame_southwest,
                            text = txt_table_option,
                            variable = self.table_option,
                            command = self.showTable,
                            value = value_table_option).grid(row = row_table_option,column = 2,sticky ='w')
        # start Plot
        self.disableEnableCheckbuttons()
        self.startPlot()

    def disableEnableCheckbuttons(self):
        """ Disable checkbuttons of aggregation level, which is not available in balance """
        # reset Checkbuttons to default value
        self.b_nodes.set(1)
        self.b_time_slices.set(1)
        self.b_construction_years.set(1)
        # get index names of balance
        index_names = self.optimization_results['Utilities']['Index_names'][self.plotted_balance_name]
        # enable/disable nodes or connections
        if ('nodes' or 'connections') in index_names:
            # enable Checkbutton
            self.checkbutton_nodes.config(state=tk.NORMAL)
        else:
            # disable Checkbutton
            self.checkbutton_nodes.config(state=tk.DISABLED)
    
        # enable/disable construction years
        if 'construction_years' in index_names:
            # enable Checkbutton
            self.checkbutton_construction_years.config(state=tk.NORMAL)
        else:
            # disable Checkbutton
            self.checkbutton_construction_years.config(state=tk.DISABLED)

        # comment toggled since of no suitable use yet
            # enable/disable time slices
            # if 'time_slices' in index_names:
            #     # enable Checkbutton
            #     self.checkbutton_time_slices.config(state=tk.NORMAL)
            # else:
            #     # disable Checkbutton
            #     self.checkbutton_time_slices.config(state=tk.DISABLED)

    def groupBalanceByIndex(self,input_balance):
        """ Group balance by level of aggregation. Index selected by checkbuttons"""
        # simplify Plot Results          
        grouped_balance = copy.deepcopy(input_balance)
        index_names = self.optimization_results['Utilities']['Index_names'][self.plotted_balance_name]
        # sum over nodes, if 'nodes' in index
        if 'nodes' in index_names and len(index_names)>1 and self.b_nodes.get()==1:
            index_names = [index_of_var for index_of_var in index_names if index_of_var != 'nodes']
            grouped_balance = grouped_balance.groupby(index_names).sum()
        # sum over connections, if 'connections' in index
        if 'connections' in index_names and len(index_names)>1 and self.b_nodes.get()==1:
            index_names = [index_of_var for index_of_var in index_names if index_of_var != 'connections']
            grouped_balance = grouped_balance.groupby(index_names).sum()
        # sum over construction years, if 'construction_years' in index
        if 'construction_years' in index_names and len(index_names)>1 and self.b_construction_years.get()==1:
            index_names = [index_of_var for index_of_var in index_names if index_of_var != 'construction_years']
            grouped_balance = grouped_balance.groupby(index_names).sum()
        grouped_balance = grouped_balance.astype(float)
        return grouped_balance
    
    def showTable(self):
        """ show table 
        
        select whether detailed or entire dataset shown in table """
        if self.table_option.get() == self.table_options[0][1]: # detailed dataset
            super().showTable(extendedDataframe= False)
        else: # entire dataset
            super().showTable(extendedDataframe= True)

    def getTitleString(self):
        """ Get Title String depended on selection of balance 
        
        Same Title as MainFrame, only 'Detailed' added """
        title_string = '{} ({})'.format(self.mainframe.title_string,self.mainframe.selected_label.title())

        return title_string

class SaveFrame:
    """ New Frame in order to save plot and Data """
    def __init__(self,parent,mainframe):
        """ sets up save frame """
        self.parent = parent
        self.parent.title('Save Plot and Data')
        self.parent.grab_set()
        self.mainframe = mainframe
        # set font
        if 'Arial' in font.families(): # if Arial available on operation system, select Arial (Corporate Design of the RWTH)
            standard_font = 'Arial'
            self.parent.option_add('*Font', standard_font + ' 9')
        # set up Message how to use save Frame
        message_save = 'Save plots and table of current data in the SecMOD DataViewer.\nSelect export format, insert filename for exports and select folder path.\nCreates export folder if not yet created.'
        tk.Label(self.parent, text=message_save, anchor = 'w',justify = 'left').grid(row=0, column = 0, columnspan = 3, sticky = 'nsew')
        # set up checkbuttons - export format
        tk.Label(self.parent,text = 'Export Format',bg = 'grey').grid(row = 1,column=0,sticky='ew')
        # xlsx
        self.b_xlsx= tk.IntVar()
        self.b_xlsx.set(0)
        # PNG
        self.b_png = tk.IntVar()
        self.b_png.set(0)
        # PDF
        self.b_pdf = tk.IntVar()
        self.b_pdf.set(0)
        # Tikz
        self.b_tikz = tk.IntVar()
        self.b_tikz.set(0)
        # checkbuttons
        self.checkbutton_xlsx = tk.Checkbutton(self.parent,text='XLSX',variable = self.b_xlsx,command = self.toggleSaveButton)
        self.checkbutton_png = tk.Checkbutton(self.parent,text='PNG',variable = self.b_png,command = self.toggleSaveButton)
        self.checkbutton_pdf = tk.Checkbutton(self.parent,text='PDF',variable = self.b_pdf,command = self.toggleSaveButton)
        self.checkbutton_tikz = tk.Checkbutton(self.parent,text='Tikz',variable = self.b_tikz,command = self.toggleSaveButton)
        self.checkbutton_xlsx.grid(row=2,column=0,sticky='ew') 
        self.checkbutton_png.grid(row=3,column=0,sticky='ew') 
        self.checkbutton_pdf.grid(row=4,column=0,sticky='ew') 
        self.checkbutton_tikz.grid(row=5,column=0,sticky='ew') 
        # set up title selection with default filename
        # default filename defined by balance
        default_filename = 'Results'
        if self.mainframe.selection_balance.get() == self.mainframe.list_balances[0]: # Product Flows
            default_filename += '_product_flow_' + self.mainframe.selection_product_flows.get()
        elif self.mainframe.selection_balance.get() == self.mainframe.list_balances[1]: # Demand
            default_filename += '_' +  self.mainframe.selection_demand.get()
        elif self.mainframe.selection_balance.get() == self.mainframe.list_balances[2]: # Capacities
            default_filename += '_existing_capacity_' + self.mainframe.selection_process.get()
        elif self.mainframe.selection_balance.get() == self.mainframe.list_balances[3]: # Impacts
            default_filename += '_' + self.mainframe.selection_impact.get() + '_impact'
            if self.mainframe.selection_impact_process.get() != self.mainframe.list_impact_process[0][1]: # by process category
                default_filename += '_' + self.mainframe.selection_impact_process.get()
        if self.mainframe.selection_product.get() != self.mainframe.list_products[0]: # single product
            default_filename += '_' + self.mainframe.selection_product.get().lower()
        tk.Label(self.parent,text = 'Select Filename',bg = 'grey').grid(row = 1,column=1,sticky='ew')
        # create entry to alter filename
        self.entry_filename = tk.Entry(self.parent)
        self.entry_filename.insert(tk.END,default_filename)
        self.entry_filename.grid(row = 2,column=1,columnspan = 2,sticky='ew')
        # set up path to save plot/data
        tk.Label(self.parent,text = 'Select Folderpath',bg = 'grey').grid(row = 3,column=1,sticky='ew')
        self.string_folderpath = tk.StringVar()
        self.string_folderpath.set(str(self.mainframe.working_directory)+'/SecMOD/01-MODEL-RESULTS')
        tk.Label(self.parent,textvariable = self.string_folderpath).grid(row = 4,column=1,columnspan = 2,sticky='ew')
        self.button_folderpath = tk.Button(self.parent,text = 'Browse Folder',command = self.browseFolder)
        self.button_folderpath.grid(row = 3,column=2,sticky='ew')
        # set up save button and message
        self.button_save = tk.Button(self.parent,text = 'Save Selection',command = self.saveSelection)
        self.button_save.grid(row = 6,column=0,sticky='ew')
        self.string_save_message = tk.StringVar()
        tk.Label(self.parent,textvariable = self.string_save_message).grid(row = 6,column=1,sticky='ew')
        # set up destroy button
        self.button_destroy =  tk.Button(self.parent,text = 'Close',command = self.parent.destroy)
        self.button_destroy.grid(row = 6,column=2,sticky='ew') 
        # toggle save button
        self.toggleSaveButton()

    def toggleSaveButton(self):
        """ disables the SaveButton if no checkbutton selected """
        # clear save message label
        self.string_save_message.set('')
        # toggle save button
        if self.b_xlsx.get() == 0 and self.b_png.get() == 0 and self.b_pdf.get() == 0 and self.b_tikz.get() == 0: # no checkbutton selected
            self.button_save.config(state=tk.DISABLED)
        else:
            self.button_save.config(state=tk.NORMAL)

    def browseFolder(self):
        """ Browses Folder and selects new Folder """
        # clear save message label
        self.string_save_message.set('')
        # get folderpath
        folderpath = tk.filedialog.askdirectory(title = 'Please select a directory')
        if folderpath:
            # if folderpath specified. If no new path selected, use default folderpath
            self.string_folderpath.set(folderpath)

    def saveSelection(self):
        """ saves the selection as <filename> in <folderpath> """
        if self.entry_filename.get() == '': # if no name specified
            showinfo('No Filename','Please specify Filename')
        else:
            if os.path.split(self.string_folderpath.get())[1] != 'Export': # if not already in Export Folder
                exportfolder = self.string_folderpath.get()+"/Export" # add Export to path
                if not os.path.exists(exportfolder): # if "Export" Folder doesn't exist
                    os.mkdir(exportfolder) # create Export Folder
            if self.b_xlsx.get() == 1: # save xlsx
                self.mainframe.balance_table.to_excel("{0}/{1}.{2}".format(exportfolder,self.entry_filename.get(), "xlsx"))
            if self.b_png.get() == 1: # save png
                self.mainframe.fig.savefig("{0}/{1}.{2}".format(exportfolder,self.entry_filename.get(), "png"))
            if self.b_pdf.get() == 1: # save pdf
                self.mainframe.fig.savefig("{0}/{1}.{2}".format(exportfolder,self.entry_filename.get(), "pdf"))
            if self.b_tikz.get() == 1: # save tikz
                plt2tikz.save("{0}/{1}.{2}".format(exportfolder,self.entry_filename.get(), "tex"),figure = self.mainframe.fig)
            logging.info('Results exported')
            # set save message
            self.string_save_message.set('Selection saved!')

def evaluate(working_directory):
    """Evaluates the results of the optimization."""
    # open optimization results
    with open(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "Extracted Results" / "Optimization_Results.pickle", 'rb') as input_file:
        optimization_results = pickle.load(input_file)
    # set up GUI
    root = tk.Tk()
    MainFrame_evaluation(root,optimization_results,working_directory)
    root.mainloop()

def start_evaluation(working_directory, debug_evaluation=False):
    helpers.log_heading("Processing results")
    # check whether Results already extracted
    if os.path.exists(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "Extracted Results" / "Date_Log_Raw_Results.pickle"):  
        with open(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "Extracted Results" / "Date_Log_Raw_Results.pickle", "rb") as input_file:
            date_creation_log = pickle.load(input_file)
        with open(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "InvestmentModel_{0}.pickle".format(config.invest_years[0]), "rb") as input_file:
            # get creation date of Raw Results:"InvestmentModel_"+year[0]+".pickle"
            if platform.system() == 'Windows':
                date_creation_result_file = os.path.getmtime(input_file.name)
            # different system
            else:
                date_creation_result_file = os.stat(input_file.name).st_mtime
        # if modification date of current result file is not the modification date of logged file
        if date_creation_log != date_creation_result_file or debug_evaluation:
            result_processing.extract_results(working_directory)
    else:
        result_processing.extract_results(working_directory)
    evaluate(working_directory)

if __name__ == "__main__":
    working_directory = Path().cwd()
    # start evaluation
    start_evaluation(working_directory,debug_evaluation=False)