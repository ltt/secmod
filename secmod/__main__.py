from pathlib import Path
import logging
import time
import os
from datetime import datetime
from pyomo.opt import TerminationCondition
import pickle

log_format = '%(asctime)s %(filename)s: %(levelname)s: %(message)s'
if not os.path.exists("logs"):
            os.mkdir("logs")
logging.basicConfig(filename='logs/secmod.log', level=logging.INFO, format=log_format,
                    datefmt='%Y-%m-%d %H:%M:%S')
logging.getLogger().addHandler(logging.StreamHandler())
logging.propagate = False

import secmod.data_preprocessing as dataprep
import secmod.setup as setup
import secmod.helpers as helpers
import secmod.data_processing as data_processing
from secmod.classes import ImpactCategory, ProcessImpacts, ProductionProcess, StorageProcess, TransshipmentProcess, TransmissionProcess, units, config
from secmod.optimization import Optimization
import secmod.evaluation as evaluation
import secmod.scaling as scaling

# Get current working directory
working_directory = Path.cwd()
setup.setup(working_directory, reset=False)

def main():
    """This is the main method of SecMOD.

    This method is called when SecMOD is called as a script instead of being imported,
    e.g. using the following command line in the command line::

        python -m secmod

    It is the main entrance point for starting a run of SecMOD.
    """

    helpers.log_heading("SecMOD - Startup")
    
    computed_input_path = working_directory / "SecMOD" / "00-INPUT" / "01-COMPUTED-INPUT"
    
    # Setup static variables
    ProcessImpacts.interest_rate = config.interest_rate
    ProcessImpacts.economic_period = units(config.economic_period)
    
    # Choose LCA framework - "ReCiPe Midpoint (H)", "ILCD 1.0.8 2016 midpoint" or "cumulative energy demand"
    ImpactCategory.FRAMEWORK = config.LCA_framework

    # Override used impact categories to reduce preparation time - "None" if FRAMEWORK should be used
    if hasattr(config, "LCA_manual_impact_categories"):
        ImpactCategory.MANUAL_IMPACT_CATEGORY_SELECTION = config.LCA_manual_impact_categories

    # Load raw data
    if config.load_raw_input:
        data_processing.load_raw_input(working_directory / "SecMOD" / "00-INPUT" / "00-RAW-INPUT")
    
    # Sort invest years in case someone put them in the wrong order
    config.invest_years.sort()

    # Go over all years using a rolling horizon for the optimization
    for reference_year in config.invest_years:
        # Select invest years for the current optimization horizon
        ProcessImpacts.invest_years = [year for year in config.invest_years if (year >= reference_year) and ((config.invest_years.index(year) - config.invest_years.index(reference_year)) < (config.invest_years_per_optimization))]
        helpers.log_heading("Start optimization horizon of {0}, including {1}".format(reference_year, ProcessImpacts.invest_years))
            
        # Generate input dictionary or start with an existing one (only senseful for debugging)
        input_dictionary = data_processing.generate_input_dictionary(computed_input_path, config.load_existing_input_dict)
        input_dictionary[None]['type_of_optimization'] = {None: ['InvestmentOptimization']}
        # Make sure that in the next optimization horizon a new input dictionary is created
        config.load_existing_input_dict = False

        # Initialize Optimization class
        thisInvestmentOptimization = Optimization()
        if not os.path.exists(working_directory / "SecMOD" / "01-MODEL-RESULTS"):
            os.mkdir(working_directory / "SecMOD" / "01-MODEL-RESULTS")

        if config.load_existing_results:
            with open(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "InvestmentModel_{0}.pickle".format(min(ProcessImpacts.invest_years)), "rb") as input_file:
                thisInvestmentOptimization = pickle.load(input_file)
                config.load_existing_results = False
        else:
            thisInvestmentOptimization.run(input_dict=input_dictionary, solver=config.solver, solver_options=config.solver_options, debug=config.debug_optimization)
            with open(working_directory / "SecMOD" / "01-MODEL-RESULTS" / "InvestmentModel_{0}.pickle".format(min(ProcessImpacts.invest_years)), "wb") as input_file:
                pickle.dump(thisInvestmentOptimization.model_instance, input_file)

        # if infeasible, compute and write Irreducible Inconsistent Subsystem (subset model with infeasible constraints)
        if config.solver == 'gurobi_persistent' and config.debug_optimization and (thisInvestmentOptimization.results.solver.termination_condition == getattr(TerminationCondition,'infeasible') or thisInvestmentOptimization.results.solver.termination_condition == getattr(TerminationCondition,'infeasibleOrUnbounded')):
            thisInvestmentOptimization.mysolver._solver_model.computeIIS()
            thisInvestmentOptimization.mysolver._solver_model.write("{0}/SecMOD/01-MODEL-RESULTS/gurobi_model_{1}.ilp".format(working_directory, min(ProcessImpacts.invest_years)))
        # add new capacity
        data_processing.add_new_capacity_from_results(thisInvestmentOptimization)

    logging.info("\n\n")
    logging.info("========================================")
    logging.info("=               THE END!               =")
    logging.info("========================================")
    # start evaluation
    evaluation.start_evaluation(working_directory)
    
    logging.getLogger().handlers.clear()
    logging.shutdown()

if __name__ == "__main__":
    main()
