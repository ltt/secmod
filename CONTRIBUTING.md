# Contributing

You are interested in contributing to SecMOD? We appreciate any contributions!

## Reporting issues

Please note that the issue tracker is not for questions. Please use other platforms, such as Stack Overflow
instead. Make sure to tag your question with SecMOD tag.

If possible, before submitting an issue report, try to verify that the issue
hasen't already been fixed and is not a duplicate.

## Submitting code

If you contribute code to SecMOD, you agree to license your code under the MIT.

The new code should follow PEP8 coding style (except the line length limit,
which is 90) and adhere to the style of the surrounding code.

You should preferably use atomic (very small) commits for your changes. Read more
about why and how [here](https://www.freshconsulting.com/atomic-commits/).

You must follow some simple formatting rules for your commit messages, which can
be found [here](https://chris.beams.io/posts/git-commit/)


