# README - 01-GRID

This directory contains all data packages which define
a grid model. A grid definition contains the information
about the nodes of the grid, their geo coordinates
and connections between the nodes. A connection is only
defined by the two nodes it connects.

SecMOD can handle multiple grid folders at once. The grid which should be used in the optimization must be indicated in the config.py.
