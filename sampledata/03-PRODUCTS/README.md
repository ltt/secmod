# README - PRODUCTS

This directory contains all data packages which define the products
used in SecMOD, containing the product name as well as nodal demand time series.
