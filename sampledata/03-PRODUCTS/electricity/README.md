# README - ELECTRICITY

This data package contains all necessary information
about the product: **electricity**. Grid-dependent information can be modified in the folder named similar to the employed grid. Further, the cost of a product can be used either as real cost for product import, or as a slack variable for debugging, when unrealistically high cost are chosen instead of entirely forbidding imports of the product. 
