# README - ECOINVENT

This data package contains the life cycle impacts for all ecoinvent processes. These processes are used to define the environmental impact of the used processes. It includes life cycle impacts following the ILCD2 midpoint method, but can be easily extended by other assessment methods given e.g., in the ecoinvent table.

All ecoinvent files contain randomly assigned exemplary values which were not taken from the ecoinvent database.
