# SecMOD Data

This project includes a small data set documented in our publication, representing case study 1. 

Due to license restrictions, all ecoinvent files contain randomly assigned exemplary values which were not taken from the ecoinvent database. If used for other projects, the ecoinvent file `04-ECOINVENT/ecoinvent.csv` and the LCIs should be updated with the given format and original data. 


