
EXTERNAL DATA PACKAGES
===========================================================================
SecMOD can include external data from the OPSD-database. The folder "scripts" contains the code we used to include data about existing conventional and renewable plants. Please note that existing infrastructure is not included automatically in the current settings.


We follow the Data Package standard by the [Frictionless Data project](http://frictionlessdata.io/),
a part of the Open Knowledge Foundation.

Field documentation
===========================================================================

external_data_packages.csv
---------------------------------------------------------------------------

* internal_name
  * Type: string
  * Description: unique and fixed name for the internal use of the external data package
* source_url
  * Type: string
  * Description: unique and fixed name for the internal use of the external data package
