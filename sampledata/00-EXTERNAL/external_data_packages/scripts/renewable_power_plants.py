# Important columns of the file renewable_power_plants_DE.csv:
# - commissioning_date
# - decommissioning_date
# - technology
# - electrical_capacity
# - lat
# - lon

import pandas as pd
import numpy as np
from geopy import distance
from pathlib import Path
from tqdm import tqdm
import secmod.helpers as helpers

def setup(working_directory: Path, data_package_name: str, grid: str = "DE", unit: str = "MW", years: list =[1900,1905,1910,1915,1920,1925,1930,1935,1940,1945,1950,1955,1960,1965,1970,1975,1980,1985,1990,1995,2000,2005,2010,2016,2020,2025,2030,2035,2040,2045,2050], debug=False):
    """This method is called in order to import the data from this data package to the input data."""

    tqdm.pandas()
    used_processes = {"Run-of-river", "Onshore", "Photovoltaics", "Geothermal", "Offshore"}
    process_to_limit_potential = {"Run-of-river", "Geothermal"}
    internal_process_name = {"Run-of-river": "run-of-river_hydro", "Onshore": "wind_onshore", "Photovoltaics": "photovoltaics", "Geothermal": "geothermal", "Offshore": "wind_offshore"}
    process_classes = {"Run-of-river": "production", "Biomass and biogas": "production", "Sewage and landfill gas": "production", "Onshore": "production", "Photovoltaics": "production", "Other fossil fuels": "production", "Geothermal": "production", "Offshore": "production", "Photovoltaics ground": "production", "Storage": "storage"}

    columns = ["commissioning_date", "decommissioning_date", "technology", "electrical_capacity", "lat", "lon"]
    print("Load data")
    plants = pd.read_csv(working_directory / "SecMOD" / "00-INPUT" / "00-RAW-INPUT" / "00-EXTERNAL" / data_package_name / "renewable_power_plants_{0}.csv".format(grid), float_precision="high", usecols=(lambda column: column in columns), parse_dates=["commissioning_date", "decommissioning_date"])
    if debug:
        plants = plants.sample(500)
    nodes = pd.read_csv(working_directory / "SecMOD" / "00-INPUT" / "00-RAW-INPUT" / "01-GRID" / grid / "nodes.csv", index_col="node", usecols=lambda x: "Unnamed" not in x, float_precision="high")
    print("Select used technologies")
    plants = plants[plants["technology"].isin(used_processes)]
    print("Determine commissioning year")
    plants["commissioning_year"]    = plants["commissioning_date"].progress_apply(lambda date: date.year)
    print("Determine decommissioning year")
    plants["decommissioning_year"]  = plants["decommissioning_date"].progress_apply(lambda date: date.year)
    plants = plants[["technology", "electrical_capacity", "lat", "lon", "commissioning_year", "decommissioning_year"]]
    print("Determine actual construction year")
    plants["construction_year"]     = plants.progress_apply(lambda plant: get_construction_year(plant, working_directory, internal_process_name[plant.technology], process_classes[plant.technology]), axis=1)
    print("Get used construction year")
    plants["construction_year"] = plants["construction_year"].progress_apply(lambda year: get_closest_year(years, year))
    plants = plants[["technology", "electrical_capacity", "lat", "lon", "construction_year"]]
    plants = plants.dropna()
    print("Determine closest nodes")
    plants["node"] = plants[["lat", "lon"]].progress_apply(lambda plant: find_closest_node(nodes, plant.lon, plant.lat), axis=1)
    plants = plants[["technology", "electrical_capacity", "node", "construction_year"]]
    plants.set_index(["technology", "node", "construction_year"], inplace=True)
    plants.sort_index(inplace=True)
    print("Aggregate capacities by technology, nodes and year of construction")
    plants = plants["electrical_capacity"].groupby(level=[0,1,2]).sum()

    print("Write existing capacity for {0}".format(list(plants.index.get_level_values("technology").unique())))
    for process in tqdm(list(plants.index.get_level_values("technology").unique())):
        capacity = plants.loc[process].unstack("construction_year").reindex(index=nodes.index).fillna(0)
        capacity_columns = list(capacity.columns)
        capacity_columns.sort()
        capacity["unit"] = unit
        capacity["comments"] = ""
        capacity.reset_index(inplace=True)
        capacity = capacity[["node", "unit"] + capacity_columns + ["comments"]]
        export_existing_capacity(capacity, working_directory, internal_process_name[process], grid, process_classes[process])
        if process in process_to_limit_potential:
            potential = get_nodal_capacity_by_latest_year(capacity, working_directory, internal_process_name[process], process_classes[process], years)
            export_potential_capacity(potential, working_directory, internal_process_name[process], grid, process_classes[process])
    print("Done!")
    
def get_nodal_capacity_by_latest_year(capacity: pd.DataFrame, working_directory: Path, process: str, process_class: str, years: list):
    relevant_years = [int(column) for column in capacity.columns if helpers.isInteger(column)]
    reference_year = max(relevant_years)
    lifetime_duration = get_lifetime_duration(working_directory, reference_year, process, process_class)
    relevant_years = [year for year in relevant_years if (reference_year - year) <= lifetime_duration]
    pd.options.mode.chained_assignment = None  # default='warn'
    potential = capacity[relevant_years]
    potential["total"] = potential.sum(axis=1)
    potential[["node", "unit"]] = capacity[["node", "unit"]]
    potential["comments"] = "Based on the total existing capacity in {0}".format(reference_year)
    for year in years:
        potential[year] = potential["total"]
    potential = potential[["node", "unit"] + years + ["comments"]]
    pd.options.mode.chained_assignment = "warn"  # default='warn'
    return potential

def get_construction_year(plant, working_directory: Path, process: str, process_class: str = "production"):
    """Returns the construction year, to be used"""
    if np.isnan(plant.decommissioning_year):
        return plant.commissioning_year
    elif not np.isnan(plant.commissioning_year):
        return (plant.decommissioning_year - get_lifetime_duration(working_directory, plant.commissioning_year, process, process_class))
    else:
        return np.nan

def find_closest_node(nodes: pd.DataFrame, longitude, latitude):
    """This method return the ID of the node, which is closest to the plant."""
    nodes["distance"] = nodes.apply(lambda node: distance.great_circle((node.latitude, node.longitude), (latitude, longitude)).km, axis=1)
    return nodes["distance"].idxmin()

def get_lifetime_duration(working_directory: Path, construction_year: int, process: str, process_class: str = "production"):
    """Return the lifetime duration of a process from lifetime_duration.csv"""
    process_class_directories = {"production": "01-PRODUCTION", "storage": "02-STORAGE", "transshipment": "03-TRANSSHIPMENT", "transmission": "04-TRANSMISSION"}
    target_directory = working_directory / "SecMOD" / "00-INPUT" / "00-RAW-INPUT" / "02-PROCESSES" / process_class_directories[process_class] / "01-ECOINVENT-BASED" / process / "lifetime_duration.csv"
    lifetime_duration = pd.read_csv(target_directory, float_precision="high", usecols=(lambda column: (column != "comments") and (column != "unit")))
    years_of_data = [int(column) for column in lifetime_duration.columns]
    lifetime_duration.columns = years_of_data
    closest_year = get_closest_year(years_of_data, construction_year)
    return lifetime_duration.at[0,get_closest_year(years_of_data, construction_year)]

def get_closest_year(years: list, year_of_construction: int):
    """Return the closest year"""
    difference = [abs(year - year_of_construction) for year in years]
    return years[difference.index(min(difference))]

def export_existing_capacity(capacity: pd.DataFrame, working_directory: Path, process: str, grid: str, process_class: str = "production"):
    """This method saves a dataframe to the correct position in a process and grid-subfolder."""
    process_class_directories = {"production": "01-PRODUCTION", "storage": "02-STORAGE", "transshipment": "03-TRANSSHIPMENT", "transmission": "04-TRANSMISSION"}
    target_directory = working_directory / "SecMOD" / "00-INPUT" / "00-RAW-INPUT" / "02-PROCESSES" / process_class_directories[process_class] / "01-ECOINVENT-BASED" / process / grid / "existing_capacity.csv"
    capacity.to_csv(path_or_buf=target_directory, index=False)

def export_potential_capacity(potential: pd.DataFrame, working_directory: Path, process: str, grid: str, process_class: str):
    """This method saves a dataframe to the correct position in a process and grid-subfolder."""
    process_class_directories = {"production": "01-PRODUCTION", "storage": "02-STORAGE", "transshipment": "03-TRANSSHIPMENT", "transmission": "04-TRANSMISSION"}
    target_directory = working_directory / "SecMOD" / "00-INPUT" / "00-RAW-INPUT" / "02-PROCESSES" / process_class_directories[process_class] / "01-ECOINVENT-BASED" / process / grid / "potential_capacity.csv"
    potential.to_csv(path_or_buf=target_directory)

if __name__ == "__main__":
    setup(Path().cwd(), "renewable_power_plants", debug=False)