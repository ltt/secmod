# README - 00-EXTERNAL

This directory contains a data package "external_data_packages", which has links
to the sources of some useful external data: the open power system data (OPSD) platform. In our current work, we included their data on existing conventional and renewable power plants in Germany. Please refer to the folder "external data packages" for the script we used.

Please note that in some cases, updates on the script might be necessary if the data format on the OPSD platform changes.

Furthermore all external data packages are downloaded to this directory. The downloaded
data packages should not be modified, since the modifications will be overwritten,
if an update of the external data packages is available from the source.
