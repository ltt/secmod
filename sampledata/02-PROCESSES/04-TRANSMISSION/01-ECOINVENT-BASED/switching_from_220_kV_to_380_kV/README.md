# README - SWITCHING FROM 220 KV TO 380 KV

This data package contains all necessary information
about the transmission process **switching from 220 kV to 380 kV**. Additional information for a specific grid can be modified in the folder named similar to the grid which is used.

All ecoinvent files contain randomly assigned exemplary values which were not taken from the ecoinvent database.
