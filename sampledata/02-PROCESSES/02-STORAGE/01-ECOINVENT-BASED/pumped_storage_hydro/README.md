# README - PUMPED STORAGE HYDRO

This data package contains all necessary information
about the storage process **pumped storage hydro**. Additional information for a specific grid can be modified in the folder named similar to the grid which is used.

All ecoinvent files contain randomly assigned exemplary values which were not taken from the ecoinvent database.
