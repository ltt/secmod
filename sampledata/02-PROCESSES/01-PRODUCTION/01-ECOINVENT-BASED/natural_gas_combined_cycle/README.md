# README - NATURAL GAS COMBINED CYCLE

This data package contains all necessary information
about the production process **natural gas combined cycle**. Additional information for a specific grid can be modified in the folder named similar to the grid which is used.

All ecoinvent files contain randomly assigned exemplary values which were not taken from the ecoinvent database.
