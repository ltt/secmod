grid_name       = "DE" #name of the used grid topology
invest_years_per_optimization = 1 #foresight (number of considered investment periods in each optimization)
invest_years = [2016,2020,2025,2030,2035,2040] #considered investment periods for the transition pathway
interest_rate   = 0.05 
economic_period = "30 years" #determines the maximal annualization horizon of a process
# Choose LCA framework - e.g., "ReCiPe Midpoint (H)", "ILCD 2.0 2018 midpoint" or "cumulative energy demand"
LCA_framework   = "ReCiPe Midpoint (H)"
# Override used impact categories to reduce preparation time - "None" if FRAMEWORK should be used
LCA_manual_impact_categories = ["cost", "ILCD 2.0 2018 midpoint:climate change:climate change total"]


# Set parameters for the timeseries aggregation, done by TSAM
# Documentation of all parameters is here https://github.com/FZJ-IEK3-VSA/tsam/blob/master/tsam/timeseriesaggregation.py
# See "class TimeSeriesAggregation"
time_series_aggregation_config = {
    "resolution"                : None,
    "noTypicalPeriods"          : 10,
    "hoursPerPeriod"            : 2,
    "clusterMethod"             : "hierarchical",
    "evalSumPeriods"            : False,
    "sortValues"                : False,
    "sameMean"                  : False,
    "rescaleClusterPeriods"     : True,
    "extremePeriodMethod"       : 'None',
    "predefClusterOrder"        : None,
    "predefClusterCenterIndices": None,
    "solver_tsa"                : "gurobi",
    "roundOutput"               : None,
    "addPeakMin"                : None,
    "addPeakMax"                : None,
    "addMeanMin"                : None,
    "addMeanMax"                : None,
    "use_time_series_aggregation": True,
    "correct_dateformat"        : '%d.%m.%Y %H:%M', # desired format of timeseries indizes
    "partially_correct_dateformat"  : '%d.%m.%Y %H:%M:%S', # partially correct format of timeseries indizes that is meant to be changed
    "wrong_dateformat"          : '%d-%b-%Y %H:%M:%S' # current format of timeseries indizes that is meant to be changed
}

# scaling parameters
scaling_options = {
    'algorithm':            'geom_mean', # 'equi': Equilibration, 'geom_mean': Geometric Mean, 'approx_geom_mean': Approximated Geometric Mean, 'arith_mean': Arithmetic Mean
    'Round_to_power_of_2':  True, # rounds scaling factors to power of two (often numerically better)
    'scale_constraints':    True, # scaled while model instantiated 
    'scale_variables':      True, # scaled after model instatiated
    'analyze_numerics':     True, # retrieves information about numerics
    'eps_bound':            1e-8  # threshold for printing constraint bound
}

# choose between gurobi_persistent, cplex, and other solvers
solver = "gurobi_persistent"
solver_options = {
            "ObjScale": 0,
            "LogToConsole": 1,
            "NumericFocus": 3,
            "ScaleFlag": 0,
            "Method": 0,
            "BarHomogeneous": 1,
            "InfUnbdInfo": 0,
            "ResultFile": "model.ilp", # writes largest feasible model, in the case that the full model is infeasible
            }

debug_optimization = True

load_raw_input = True #loads input from working directory, if fals, existing input must be loaded
load_existing_input_dict = False #loads input from existing input dictionary
load_existing_results = False #loads results from previous calculation
new_pint_units = "./SecMOD/00-INPUT/00-RAW-INPUT/00-UNITS-DEFINITIONS.txt" #Units employed in the model

fix_slack_variables = True #, if the non_served_demand of the in config.fix_products specified products should be fixed to 0.
# fix_slack_variables = False allows to import products at the cost given in the products folder any may be used as slack variable for debugging.
fix_products = ["electricity"]

