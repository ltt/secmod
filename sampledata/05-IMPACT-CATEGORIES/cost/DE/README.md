# README - IMPACT CATEGORY - cost - DE

This directory contains all necessary information about this
impact category, including the nodal and total impact limits in the grid DE. Impact limits can be defined on a nodal or a total level for either the total impacts, or operational or invest impacts only.
