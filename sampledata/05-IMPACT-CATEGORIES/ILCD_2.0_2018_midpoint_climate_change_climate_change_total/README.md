# README - IMPACT CATEGORY - ILCD 2.0 2018 midpoint:climate change:GWP 100a

This directory contains all necessary information about this
impact category, including the nodal and total impact limits. The objective factor indicates whether this category is part of the objective function. The overshoot allows to exceed given impact limits at a penalty. 
Grid-dependent information can be modified in the folder with the same name as the grid.
