# README - IMPACT CATEGORY - ILCD 2.0 2018 midpoint:climate change:GWP 100a - DE

This directory contains all necessary information about this
impact category, including the nodal and total impact limits in the grid DE. 
Impact limits can be defined on a nodal or a total level for either the total impacts, or operational or invest impacts only.
